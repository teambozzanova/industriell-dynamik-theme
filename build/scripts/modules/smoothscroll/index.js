const SmoothScroll = function() {
	const toTop = document.querySelector('a[href="#body"]');

	if(toTop) {
		toTop.addEventListener('click', (event) => {
			event.preventDefault();

			scroll({
				top: 0,
				behavior: 'smooth'
			});
		});
	}

};

export default SmoothScroll;
