import List from 'list.js';

function MembersFilter() {
	// eslint-disable-next-line
	const search = document.querySelector('input[type="text"].search');

	let options = {
		valueNames: [
			{
				name: 'query',
				attr: 'data-query'
			},
		]/*,
		page: 9,
		pagination: true,
		paginationClass: 'pagination'*/
	}

	// eslint-disable-next-line
	let membersList = new List('members-list', options);

	/*membersList.on('searchComplete', function () {
		updateCount(membersList.update().visibleItems.length);
	});

	/*if(searchBtn) {
		searchBtn.addEventListener('click', () => {
			membersList.search(search.value);
		});
	}

	/f(search) {
		search.onkeyup = () => {
			if(search.value === '') {
				membersList.search(search.value);
			}
		};
	}*/
}

export default MembersFilter;
