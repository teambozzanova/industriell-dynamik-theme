const HeroSidebar = function() {
	const heroSidebarBtn = document.getElementById('sidebar-hamburger-btn');
	const heroSidebar = document.getElementById('hero-sidebar');

	if(heroSidebar) {

		if (heroSidebarBtn) {
			heroSidebarBtn.addEventListener('click', (event) => {
				event.preventDefault();
				event.stopPropagation();
				const heroSidebarMode =
				heroSidebar.getAttribute('aria-expanded') !== 'false'
						? 'false'
						: 'true';
				heroSidebar.setAttribute('aria-expanded', heroSidebarMode);
			});
		}
	}
};

export default HeroSidebar;
