import List from 'list.js';

function SearchFilter() {

	let options = {
		valueNames: [],
		page: 7,
		pagination: true,
		paginationClass: 'pagination',
	}

	// eslint-disable-next-line
	let searchList = new List('search-list', options);
}

export default SearchFilter;
