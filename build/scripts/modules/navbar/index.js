const Navbar = function() {
	const rootEl = document.getElementsByTagName('html')[0];
	const navbarBtn = document.getElementById('hamburger-btn');
	const navbarCloseBtn = document.getElementById('navbar-close-btn');
	const subMenuTriggers = Array.from(document.querySelectorAll('nav.navbar li[aria-expanded] .menu-item-header'));

	function closeNavbar() {
		rootEl.setAttribute('data-navbar-expanded', 'false');
	}

	function closeSubMenus(excludedMenu) {
    subMenuTriggers.forEach(subMenu => {
      const parent = subMenu.closest('li[aria-expanded]');

      if(parent !== excludedMenu) {
        parent.setAttribute('aria-expanded', false);
      }
    });
	}

	if (navbarCloseBtn) {
		navbarCloseBtn.addEventListener('click', () => {
			closeNavbar();
		});
	}

	if (navbarBtn) {
		navbarBtn.addEventListener('click', (event) => {
			event.preventDefault();
			event.stopPropagation();
			//closeSearchModal();
			const navbarMode =
			rootEl.getAttribute('data-navbar-expanded') !== 'false'
					? 'false'
					: 'true';
			rootEl.setAttribute('data-navbar-expanded', navbarMode);
		});
	}

	if(subMenuTriggers) {
    subMenuTriggers.forEach(trigger => {
      trigger.addEventListener('click', (event) => {
				event.preventDefault();
				event.stopPropagation();
        const parent = trigger.closest('li[aria-expanded]');

        if(parent) {
          let mode = (parent.getAttribute('aria-expanded') === 'true') ? true : false;
          parent.setAttribute('aria-expanded', !mode);
					closeSubMenus(parent);
        }
      });
    });
  }

};

export default Navbar;
