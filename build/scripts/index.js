/* eslint-disable no-unused-vars */
import Navbar from './modules/navbar';
import LoadModule from './modules';
import CheckSelector from './utils/checkselector';
import CookieConsent from './modules/cookie-consent';
import './modules/objectfit';

window.addEventListener('load', () => {
	Navbar();
	CookieConsent();
	LoadModule('smoothscroll', CheckSelector('a[href="#body"]'));

	if(CheckSelector('#members-list')) {
		LoadModule('members-filter');
	}

	if(CheckSelector('#search-list')) {
		LoadModule('search-filter');
	}

	if(CheckSelector('#hero-sidebar')) {
		LoadModule('hero-sidebar');
	}
});
