<?php
/**
 * The template for displaying archive pages.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 */

namespace Industrielldynamik;

$is_member_organizations_archive = is_post_type_archive( registry_get( 'member_organizations' )->get_posttype_name() );
$is_success_stories_archive      = is_post_type_archive( registry_get( 'success_stories' )->get_posttype_name() );

get_header();

if ( true === $is_member_organizations_archive ) {
	echo get_theme_template('template-parts/layouts/archive/network_archive');
}

if ( true === $is_success_stories_archive ) {
	echo get_theme_template('template-parts/layouts/archive/success_stories_archive');
}

get_footer();
