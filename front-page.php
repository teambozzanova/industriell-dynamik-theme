<?php
/* Template Name: Startsida */

/**
 * The template for displaying the front-page.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#front-page-display
 * @since   1.0.0
 */

namespace Industrielldynamik;

use function get_footer;
use function get_header;

$featured_cards       = registry_get( 'acf' )->get_field( 'featured_cards' );
$preamble             = registry_get( 'acf' )->get_field( 'preamble' );
$member_organizations = registry_get( 'acf' )->get_field( 'member_organizations' );

?>

<?php get_header(); ?>

<?php if ( $featured_cards ) : ?>
	<?= get_theme_template( 'template-parts/partials/featured_cards/featured_cards', ['featured_cards' => $featured_cards] ); ?>
<?php endif; ?>

<?php if ( $preamble || $member_organizations ) : ?>
	<?= get_theme_template( 'template-parts/partials/member_organizations/member_organizations', ['preamble' => $preamble, 'member_organizations' => $member_organizations] ); ?>
<?php endif; ?>

<?php get_footer(); ?>
