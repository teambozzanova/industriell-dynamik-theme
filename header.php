<?php
/**
 * The header for industrielldynamik Theme.
 *
 * This is the template that displays all of the <head> section and everything
 * up until <main role="main">.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @since   1.0.0
 */

namespace Industrielldynamik;

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js" data-navbar-expanded="false" data-search-modal-expanded="false">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script>document.querySelector("html").classList.remove("no-js");</script>
		<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5F2HQW9');</script>
<!-- End Google Tag Manager -->
		<?php wp_head(); ?>
		<link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
		<!--link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,400;0,700;1,700&display=swap" rel="stylesheet"-->
		<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	</head>
	<body id="body" <?php body_class( 'relative flex flex-col min-h-screen' ); ?>>
		<?php wp_body_open(); ?>
		<noscript id="noscript">
			<p><?= __( 'This page requires JavaScript, please enable it in your browser to access the content.', 'industrielldynamik' ); ?></p>
		</noscript>
		
		<!--[if lte IE 9]><p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.'</p><![endif]-->
		<?php do_action( 'before_main' ); ?>
		<main role="main" id="app">
