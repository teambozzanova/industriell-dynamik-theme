<?php
/**
 * The good examples page template file.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#front-page-display
 * @since   1.0.0
 */

namespace Industrielldynamik;

$active_category = isset( $_GET['cat'] ) ? sanitize_key( $_GET['cat'] ) : null;

$success_stories_query = [
	'orderby' => 'date',
	'order'   => 'DESC',
];

if ( $active_category ) {
	$success_stories_query['tax_query'] = [
		[
			'taxonomy' => registry_get( 'success_story_categories')->get_plural_name(),
			'terms'    => $active_category,
			'field'    => 'slug',
		],
	];
}

$success_stories = registry_get( 'success_stories' )->get_success_story_items( $success_stories_query );

?>

<section class="bg-white">
	<div class="container px-2 md:px-10 pt-10 pb-24 flex flex-row flex-wrap">
		<?php if ( $success_stories ) : ?>
			<div class="px-6 py-10 w-full text-h6">
				<span class="font-semibold">
					<?= __( 'Showing', 'industrielldynamik' ); ?> <?= count( $success_stories ) ?> <?= ( count( $success_stories ) > 1 ) ? __( 'articles', 'industrielldynamik' ) : __( 'article', 'industrielldynamik' ); ?>
				</span>
			</div>
			<?php foreach ( $success_stories as $success_story ) : ?>
				<article class="mb-6 md:mb-0 px-6 py-4 w-full md:w-1/2 lg:w-1/3 xl:w-1/3 success-story-card <?= ( isset( $success_story['success_stories_link'] ) ) ? 'transition-opacity duration-500 ease-linear opacity-100 hover:opacity-75' : null; ?>">
					<a class="flex flex-col h-full" href="<?= $success_story['success_stories_link'] ?? '#'; ?>">
						<figure class="relative block pb-1_1 overflow-hidden bg-black">
							<?php if ( isset( $success_story['success_stories_thumbnail']['src'] ) ) : ?>
								<?= get_theme_template( 'template-parts/partials/image', [ 'image' => $success_story['success_stories_thumbnail'] ] ); ?>
							<?php endif; ?>
							<figcaption class="success-story-label mt-auto absolute text-white bottom-0 mb-6 ml-6 max-w-xl"><?= $success_story['success_stories_name'] ?? null; ?></figcaption>
						</figure>
					</a>
				</article>
			<?php endforeach; ?>
		<?php else : ?>
			<div class="w-full  md:min-h-64 flex flex-col">
				<p class="text-h6 text-center my-auto"><?= __( 'There are currently no published', 'industrielldynamik' ); ?> <?= registry_get( 'success_stories' )->get_posttype_name( true ) . '.'; ?> </p>
			</div>
		<?php endif; ?>
	</div>
</section>
