<?php
/**
 * The networks page template file.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#front-page-display
 * @since   1.0.0
 */

namespace Industrielldynamik;

use WP_Query;

$member_organizations_query = [
	'orderby' => 'title',
	'order'   => 'ASC',
];

$member_organizations = registry_get( 'member_organizations' )->get_member_organization_items( $member_organizations_query );
?>

<section class="bg-white">
	<div class="container px-2 md:px-10 pt-10 pb-24 flex flex-row flex-wrap">
		<?php if ( $member_organizations ) : ?>
			<?php /*<div class="px-6 py-10 w-full text-h6 list-count">
				<span class="font-semibold">
					<?= __( 'Showing', 'industrielldynamik' ); ?> <?= count( $member_organizations  ) ?> <?= ( count( $member_organizations ) > 1 ) ? __( 'articles', 'industrielldynamik' ) : __( 'article', 'industrielldynamik' ); ?>
				</span>
			</div> */ ?>
			<?php foreach ( $member_organizations as $member_organization ) : ?>
				<?php if ( isset( $member_organization['member_organization_thumbnail']['src'] ) ) : ?>
					<article class="px-6 py-4 w-full sm:w-1/2 md:w-1/3 lg:w-1/4 member-organization-card <?= ( isset( $member_organization['member_organization_link'] ) ) ? 'transition-opacity duration-500 ease-linear opacity-100 hover:opacity-50' : null; ?>">
						<a class="bg-gray-100 text-gray-300 p-8 flex flex-col h-full" href="<?= $member_organization['member_organization_link'] ?? '#'; ?>">
							<figure class="relative mt-6 mb-8 block pb-9_16">
								<?= get_theme_template( 'template-parts/partials/image', [ 'image' => $member_organization['member_organization_thumbnail'] ] ); ?>
							</figure>
							<small class="organizaion-name-label mt-auto text-gray-800 font-poppins font-semibold"><?= $member_organization['member_organization_name'] ?? null; ?></small>
						</a>
					</article>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php else : ?>
			<div class="w-full md:min-h-64 flex flex-col">
				<p class="text-h6 text-center my-auto"><?= __( 'There are currently no published', 'industrielldynamik' ); ?> <?= registry_get( 'member_organizations' )->get_posttype_name( true ) . '.'; ?> </p>
			</div>
		<?php endif; ?>
	</div>
</section>

<?php
$partners = new WP_Query( [
	'post_type' => 'partners',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order'   => 'ASC',
]); 
// echo '<pre>'; print_r( $partners ); echo '</pre>';

if ( $partners->have_posts() ) : ?>
<section class="partners bg-white">
	<div class="container flex"><h2>Partners</h2></div>
	<div class="container px-2 md:px-10 pt-10 pb-24 flex flex-row flex-wrap">
		
	
	<?php while ( $partners->have_posts() ) :
		$partners->the_post(); 
		$url = get_field( 'url' );
		$logo = get_field( 'logo' );
		$partner_logo      = ( $logo ) ? get_responsive_image( 
			(int) $logo, 
			'landscape-sm', 
			['class' => 'partner-logo absolute w-full h-auto absolute inset-0 m-auto flex items-center justify-center object-contain'] ) 
		: null;
	?>
		<?php if ( $logo = get_field( 'logo' ) ) : ?>
			<article class="partner px-6 py-4 w-full sm:w-1/2 md:w-1/3 lg:w-1/4 member-organization-card <?= ( isset( $url ) ) ? 'transition-opacity duration-500 ease-linear opacity-100 hover:opacity-50' : null; ?>">
				<a class="bg-gray-100 text-gray-300 p-8 flex flex-col h-full" href="<?= esc_url($url) ?? '#'; ?>" target="_blank">
					<figure class="relative mt-6 mb-8 block pb-9_16">
						<?= get_theme_template( 'template-parts/partials/image', [ 'image' => $partner_logo ] ); ?>
					</figure>
					<small class="organizaion-name-label mt-auto text-gray-800 font-poppins font-semibold"><?php the_title(); ?></small>
				</a>
			</article>
		<?php endif; ?>
	<?php endwhile; ?>
	</div>
</section>
<?php endif;  ?>