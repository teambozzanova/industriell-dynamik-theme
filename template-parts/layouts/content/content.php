<?php
/**
 * The loop/content template file.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 */

namespace Industrielldynamik;

use function have_posts;
use function the_post;

?>

<?php if ( have_posts() ) : ?>
	<section class="content container">
		<?php while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
	</section>
<?php endif; ?>
