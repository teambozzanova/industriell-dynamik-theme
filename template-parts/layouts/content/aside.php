<?php
/**
 * The loop/content template file.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 */

namespace Industrielldynamik;

use function have_posts;
use function the_post;

$sidebar          = get_field( 'side_content' );


if ( ! is_array($sidebar ) ) {
	return;
}

echo '<aside class="content-aside content">';

foreach( $sidebar as $aside ) :
	switch ( $aside['acf_fc_layout'] ) :
		case 'image':
			$image = $aside['image'];
			//echo '<pre>'; print_r( $image ); echo '</pre>';
			printf(
				'<section class="aside-image content">%s</section>',
				wp_get_attachment_image( $image, 'medium' )
			);
		break;
	endswitch;
endforeach;
echo '</aside>';
