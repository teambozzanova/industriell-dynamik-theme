<?php if ( in_array( $link_type, ['internal', 'external', 'email', 'phone'], true ) && isset( $content ) ) : ?>

	<?php
	$href = '';

	switch ( $link_type ) {
		case 'internal':
			$href = $link_internal;
			break;
		case 'external':
			$href = $link_external;
			break;
		case 'email':
			$href = "mailto:{$link_email}";
			break;
		case 'phone':
			$href = "tel:{$link_phone}";
			break;
	}
	?>

<a href="<?= $href; ?>" <?= ( 'external' === $link_type ) ? ' rel="noopener" target="_blank"' : null; ?> class="<?= $class ?? null; ?>" <?= $attr ?? null; ?>>
	<?= $content ?? null; ?>
</a>
<?php endif; ?>
