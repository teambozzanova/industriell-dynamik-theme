<?php
/**
 * The featured_cards template file.
 *
 * @package industrielldynamik;
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 */

namespace Industrielldynamik;

?>

<?php if ( $advisors_premable || $advisors ) : ?>
	<section id="members-list" class="bg-lime-100">

		<?php if ( $advisors_premable ?? null ) : ?>
			<div class="content container px-8 md:px-16 pt-20 <?= ( ! $advisors ) ? 'pb-32' : null; ?>">
				<?= $advisors_premable; ?>

				<label for="search" class="label w-full md:w-4/5 relative my-6">
					<svg aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="absolute top-0 bottom-0 right-0 w-12 h-12 mt-auto mb-auto mr-8 pointer-events-none text-green-300"><path fill="currentColor" d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z"></path></svg>
					<input type="text" id="search" class="search input bg-white pr-24 text-gray-900" placeholder="<?= __( 'Sök på namn, företag eller organisation', 'industrielldynamik' ); ?>">
				</label>
			</div>
		<?php endif; ?>

		<?php if ( $advisors ?? null ) : ?>
			<div class="list container px-4 md:px-8 py-20 flex flex-col md:flex-row flex-wrap">
				<?php foreach ( $advisors as $advisor ) : ?>
					<?= get_theme_template( 'template-parts/partials/member_cards/member_cards',
					[
						'member_thumbnail'       => $advisor['member_thumbnail'],
						'member_name'            => $advisor['member_name'],
						'member_title'           => $advisor['member_title'],
						'member_organization_id' => $advisor['member_organization_id'],
						'member_email'           => $advisor['member_email'],
						'member_phone_number'    => $advisor['member_phone_number'],
						'filterable'             => true,
						'columns'                => 'md:w-1/3 lg:w-1/4',
					] ); ?>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</section>
<?php endif; ?>
