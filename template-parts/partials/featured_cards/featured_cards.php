<?php
/**
 * The featured_cards template file.
 *
 * @package industrielldynamik;
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 */

namespace Industrielldynamik;

?>

<?php if ( $featured_cards ) : ?>
	<section class="bg-white">
		<div class="masonry container px-4 md:px-8 py-20">
			<?php foreach ( $featured_cards as $featured_card ) :
				$page_link  = '';
				$page_title = '';

			switch ( $featured_card['page_type'] ) {
					case 'single':
						$page_link  = get_the_permalink( $featured_card['link'] );
						$page_title = get_the_title( $featured_card['link'] );
					break;
					case 'archive-success-strories':
						$page_link  = get_post_type_archive_link( registry_get( 'success_stories' )->get_posttype_name() );
						$page_title = registry_get( 'success_stories' )->get_plural_name( true );
					break;
					case 'archive-member-organizations':
						$page_link  = get_post_type_archive_link( registry_get( 'member_organizations' )->get_posttype_name() );
						$page_title = registry_get( 'member_organizations' )->get_plural_name( true );
					break;
				}


				$featured_card['background_crop'] = implode(' ', array_values( $featured_card['background_crop'] ?? [] ) );
				?>

				<article class="p-4 featured-card relative overflow-hidden <?= $background_color ?? null; ?> <?= ( $featured_card['image'] ) ? null : 'no-image'; ?>  <?= $featured_card['format'] ?? null; ?> <?= $featured_card['text_color'] ?? null; ?>">
					<a href="<?= $page_link ?? '#'; ?>">
						<figure class="relative <?= $featured_card['background_color'] ?? null; ?>">

							<?php if ( $featured_card['image'] ?? null ) : ?>
								<?= get_theme_template( 'template-parts/partials/image', [ 'image' => get_responsive_image( (int) $featured_card['image'], "{$featured_card['format']}-lg", ['class' => "w-full h-full inset-0 m-auto object-cover {$featured_card['background_crop']}"] ) ] ); ?>
							<?php endif; ?>

							<?php if ( $featured_card['image_filter'] ?? null ) : ?>
								<?php if ( 'none' !== $featured_card['image_filter'] ) : ?>
									<?= get_theme_template( 'template-parts/partials/image_filter', ['filter_class' => $featured_card['image_filter'], 'opacity' => (int) $featured_card['image_filter_opacity']] ); ?>
								<?php endif; ?>
							<?php endif; ?>

							<figcaption class="absolute w-full h-full inset-0 flex flex-col">
								<?php if ( $featured_card['link'] ?? null ) : ?>
									<span class="featured-card-title font-poppins py-8 px-8 lg:px-12 mb-auto block uppercase text-xl tracking-wider mb-0"><?= $page_title ?? null; ?></span>
								<?php endif; ?>

								<?php if ( $featured_card['text'] ?? null ) : ?>
									<div class="h-full flex py-6 px-8 lg:px-12">
										<span class="featured-card-text font-poppins text-h5 pb-0 inline-block mt-auto"><?= wpautop($featured_card['text']); ?></span>
									</div>
								<?php endif; ?>
							</figcaption>
						</figure>
					</a>
				</article>
			<?php endforeach; ?>
		</div>
	</section>
<?php endif; ?>
