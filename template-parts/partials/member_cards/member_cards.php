<?php
/**
 * The member_card template file.
 *
 * @package industrielldynamik;
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 */

namespace Industrielldynamik;

$filterable = $filterable ?? false;
$columns = $columns ?? 'md:w-1/2 lg:w-1/3';

?>

<article class="p-4 md:px-8 mb-16 contact-card w-full <?= $columns; ?> <?= ( true === $filterable ) ? 'fade-in' : null; ?>">
	<?php if ( $member_thumbnail ?? null ) : ?>
		<figure class="mb-8">
			<?= get_theme_template( 'template-parts/partials/image', [ 'image' => $member_thumbnail ] ); ?>
		</figure>
	<?php endif; ?>

	<?php if ( $member_name ?? null ) : ?>
		<h5 class="pb-0 font-semibold"><?= $member_name; ?></h5>
	<?php endif; ?>

	<?php if ( $member_title ?? null ) : ?>
		<?php if ( true !== $filterable  ) : ?>
			<p class="pb-0 lg:text-2xl"><?= $member_title; ?></p>
		<?php endif; ?>
	<?php endif; ?>

	<?php if ( $member_organization_id ?? null ) : ?>
		<p class="pb-0 lg:text-2xl"><?= get_the_title( $member_organization_id ); ?></p>
	<?php endif; ?>

	<div class="contact-details pt-12">
		<?php if ( $member_email ?? null ) : ?>
			<p class="pb-0 ">
				<?= get_theme_template( 'template-parts/partials/link', ['link_type' => 'email', 'link_email' => $member_email, 'content' => __('Email Me!', 'industrielldynamik'), 'class' => 'text-link primary-color font-semibold dont-break-out lg:text-2xl'] ); ?>
			</p>
		<?php endif; ?>

		<?php if ( $member_phone_number ?? null ) : ?>
			<p class="pb-0 ">
				<?= get_theme_template( 'template-parts/partials/link', ['link_type' => 'phone', 'link_phone' => $member_phone_number, 'content' => $member_phone_number, 'class' => 'text-link lg:text-2xl'] ); ?>
			</p>
		<?php endif; ?>
	</div>
	<?php if ( true === $filterable ) : ?>
		<i class="query" data-query="<?= $member_name . ' ' . get_the_title( $member_organization_id ); ?>"></i>
	<?php endif; ?>
</article>
