<?php if ( ! isset( $_COOKIE['accept-cookies'] ) && isset( $site_options['cookie_consent']['text'] ) ) : ?>
	<aside id="using-cookies" class="fixed left-0 right-0 bottom-0 m-auto z-10 px-8 py-16 md:px-16 lg:px-40 flex flex-col md:flex-row md:justify-center bg-black text-white">
		<div class="cookies-text mb-8 md:mb-0 w-full md:w-3/4 flex flex-col justify-center content"> <?= $site_options['cookie_consent']['text'] ?? null; ?></div>

		<div class="w-full md:w-1/4">
			<button id="accept-cookies" class="btn btn-lime md:transition-colors md:duration-300 md:ease-linear"><?= ( $site_options['cookie_consent']['button_text'] ) ? $site_options['cookie_consent']['button_text'] : __( 'I accept', 'industrielldynamik' ); ?></button>
		</div>
	</aside>
<?php endif; ?>
