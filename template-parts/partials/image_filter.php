<span class="image-filter w-full h-full absolute inset-0 pointer-events-none block <?= $filter_class ?? null; ?>" style="opacity: <?= $opacity ?? 100; ?>%;"></span>
