<?php
/**
 * The template for displaying the search form.
 *
 * @package industrielldynamik
 * @author  "Eduardo Jönnerstig <eduardo_jonnerstig@live.com>"
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://github.com/WordPress/WordPress/blob/master/wp-content/themes/twentyeleven/searchform.php
 * @since   1.0.0
 */

$home_url = home_url( '/' );

?>

<form method="get" id="searchform" action="<?php echo esc_url( $home_url ); ?>">

	<?php if ( $search_label ?? null ) : ?>
		<label for="s" class="assistive-text <?= ( $search_label_class ?? null ); ?>"><?= $search_label; ?></label>
	<?php endif; ?>

	<div class="flex">
		<input type="text" class="<?= ( $search_input_class ?? null ); ?>" aria-label="<?= __( 'Search...', 'industrielldynamik' ); ?>" name="s" id="s" placeholder="<?= $search_placeholder ?? null; ?>" value="<?= $search_value ?? null; ?>" />
		<button type="submit" class="<?= ( $search_button_class ?? null ); ?>" name="submit" aria-label="<?= __( 'Search...', 'industrielldynamik' ); ?>" id="searchsubmit">
			<span class="icon-wrapper w-12 h-12 mx-8 flex justify-center">
				<svg aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="earch w-full h-auto"><path fill="currentColor" d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z"></path></svg>
			</span>
		</button>
	</div>
</form>
