<?php
/**
 * The template for displaying search results cards.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 * @since   1.0.0
 */

namespace Industrielldynamik;

?>

<?php if ( $search_result ) : ?>
	<a href="<?= $search_result['permalink']; ?>" class="w-full block bg-white p-12 shadow-xl hover:shadow-md transition-all ease-in-out duration-500 mb-16">
			<?php if ( $search_result['post_title'] ?? null ) : ?>
				<h2 class="text-h5 text-green-300 flex flex-col md:flex-row md:items-center w-full">
					<?= $search_result['post_title']; ?>
					<?php if ( $search_result['pretty_post_type'] ?? null ) : ?>
						<span class="text-gray-800 text-2xl md:ml-auto capitalize"><?= $search_result['pretty_post_type']; ?></span>
					<?php endif; ?>
				</h2>
			<?php endif; ?>

			<?php if ( $search_result['post_excerpt'] ?? null ) : ?>
				<p>
					<?= $search_result['post_excerpt']; ?>
				</p>
			<?php endif; ?>
	</a>
<?php endif; ?>
