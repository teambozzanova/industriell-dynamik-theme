<?php
/**
 * The header logo template file.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @since   1.0.0
 */

namespace Industrielldynamik;

?>

<?php if ( $image = ( true === is_integer( $logo['id'] ) ) ? get_responsive_image( $logo['id'], '', ['class' => 'header-logo'] ) : null ) : ?>
	<figure id="header-logo" class="mr-auto my-auto lg:pb-4 lg:my-0 order-first">
		<?= get_theme_template( 'template-parts/partials/link', [
			'link_type'     => 'internal',
			'link_internal' => $logo['href'],
			'class'         => 'inline-block max-w-xxs',
			'attr'          => 'aria-label="' . __( 'Home', 'nova' ) . '" title="' .  __( 'Home', 'nova' ) . '"',
			'content'       => get_theme_template( 'template-parts/partials/image', ['image' => $image] ),
			] ); ?>
	</figure>
<?php endif; ?>
