<?php
/**
 * The featured_cards template file.
 *
 * @package industrielldynamik;
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 */

namespace Industrielldynamik;

$yoast = registry_get( 'yoast' );

?>

<section class="bg-black">
	<div class="container block px-8 md:px-16 py-6">
		<?php $yoast->breadcrumbs( 'breadcrumbs', 'pb-0 text-white text-2xl' ) ?>
	</div>
</section>
