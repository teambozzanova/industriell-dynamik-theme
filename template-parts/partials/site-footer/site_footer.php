<?php
/**
 * The footer template file.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @since   1.0.0
 */

namespace Industrielldynamik;

?>
<footer id="site-footer" class="mt-auto bg-gray-800">

	<div class="container py-32 flex flex-col md:flex-row text-gray-200">

		<article class="w-full mb-16 lg:mb-0 lg:w-1/3 pl-8 pr-8 md:pl-16 md:pr-16 text-white">
			<?= get_theme_template( 'template-parts/partials/logo/header_logo', ['logo' => registry_get( 'logo' )->get_custom_logo()] ); ?>
		</article>

		<?php if ( $preamble ?? null ) : ?>
			<article class="footer-preamble w-full mb-8 lg:mb-0 lg:w-1/3 pl-8 pr-8 md:pr-0 md:pl-0">
				<?= $preamble; ?>
			</article>
		<?php endif; ?>


			<article class="w-full mb-0 lg:mb-0 lg:w-1/3 pl-8 pr-8 md:pr-16 md:pl-16 flex flex-row">
			<?php if ( $social_medias ?? null ) : ?>
				<div>
					<h6 class="text-h5 text-lime-900"><?= __( 'Follow us', 'industrielldynamik' ); ?></h6>
					<ul>
						<?php foreach ( $social_medias as $social_media ) :
							$content = $social_media['text'];
							if ( $icon = get_svg_icon( $social_media['icon'] ) ) {
								$content = "<span class=\"flex w-10 mr-5\">{$icon}</span> {$content}";
							}
						?>
						<li class="mb-8">
							<?= get_theme_template( 'template-parts/partials/link', [
								'link_type'     => 'external',
								'class'         => 'text-link flex flex-row items-center md:hover:text-white md:ease-linear md:duration-300 md:transition-colors',
								'link_external' => $social_media['url'],
								'content'       => $content,
								] ); ?>
						</li>
						<?php endforeach; ?>
					</ul>
				</div>

				<a href="#body" class="w-20 h-20 ml-auto lg:mr-auto">
					<span id="to-top" class="text-lime-900 cursor-pointer md:hover:text-white md:ease-linear md:duration-300 md:transition-colors">
						<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" version="1.1" aria-hidden="true" viewBox="0 0 448 512" xml:space="preserve" class="w-full h-auto arrow">
							<path fill="currentColor" d="M4.465 263.536l7.07 7.071c4.686 4.686 12.284 4.686 16.971 0L207 92.113V468c0 6.627 5.373 12 12 12h10c6.627 0 12-5.373 12-12V92.113l178.494 178.493c4.686 4.686 12.284 4.686 16.971 0l7.07-7.071c4.686-4.686 4.686-12.284 0-16.97l-211.05-211.05c-4.686-4.686-12.284-4.686-16.971 0L4.465 246.566c-4.687 4.686-4.687 12.284 0 16.97z"/>
						</svg>
					</span>
				</a>

				<?php endif; ?>
			</article>
	</div>

	<aside id="copyright" class="bg-black text-gray-200">
		<small class="container block px-8 md:px-16 py-12">
			<?php if ( $copyright ?? null ) : ?>
				<?= $copyright; ?>
			<?php endif; ?>

			<?php if ( $privacy_policy_url = get_privacy_policy_url() ?? null ) : ?>
				<a href="<?= $privacy_policy_url; ?>" class="text-link"><?= __( 'Cookies', 'industrielldynamik' ); ?></a> |
			<?php endif; ?>

			<?php if ( $developed_by ?? null ) : ?>
				<?= $developed_by; ?>
			<?php endif; ?>
		</small>
	</aside>
</footer>

