<?php if ( 'image/svg+xml' === $image['mime_type'] ) : ?>
	<picture title="<?= $image['title'] ?? null; ?>" alt="<?= $image['alt'] ?? null; ?>" class="<?= $image['class'] ?? null; ?>">
		<source type="<?= $image['mime_type']; ?>" srcset="<?= $image['src'] ?? null; ?>">
		<img src="<?= $image['src'] ?? null; ?>" class="style-svg" >
	</picture>
<?php else : ?>
	<img src="<?= $image['src'] ?? null; ?>" srcset="<?= $image['src_set'] ?? null; ?>" sizes="<?= $image['sizes'] ?? null; ?>" title="<?= $image['title'] ?? null; ?>" alt="<?= $image['alt'] ?? null; ?>" class="<?= $image['class'] ?? null; ?>">
<?php endif; ?>
