<?php
/**
 * The primary navigation template file.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @since   1.0.0
 */

namespace Industrielldynamik;

?>
<?php if ( $primary_navigation ?? null ) : ?>
	<ul role="menubar" class="menu order-1 w-screen block fixed right-0 overflow-y-scroll lg:py-0 lg:flex lg:top-auto lg:left-auto lg:right-auto lg:h-full lg:w-auto lg:static lg:bg-transparent lg:overflow-y-visible lg:my-0 lg:mx-0">

	<?= get_theme_template( 'template-parts/partials/navigation/navbar_close_button' ); ?>

		<?php foreach ( $primary_navigation as $menu_item ) : $is_mega_menu = ( count( $menu_item['children'] ?? [] ) > 10 ); ?>
			<li role="none" class="<?= ( ! empty($menu_item['children']) ) ? 'cursor-pointer' : null; ?> <?= $menu_item['classes']; ?> bg-transparent lg:relative <?= ($is_mega_menu) ? 'mega-menu' : null; ?>" <?= ( ! empty($menu_item['children']) ) ? 'aria-expanded="false"' : null; ?>>
				<span class="pt-6 pb-6 lg:pb-10 px-8 md:px-16 lg:px-8 lg:h-full lg:flex lg:items-end lg:justify-center lg:hover:opacity-75  <?= ( ! empty($menu_item['children']) ) ? 'menu-item-header flex items-center w-full justify-between' : 'inline-flex'; ?>">
					<a href="<?= ( ! empty($menu_item['children']) ) ? '#!' : $menu_item['url']; ?>" <?= ( $menu_item['target'] ) ? 'target="' . $menu_item['target'] . '"' : null; ?> role="menuitem" class="font-light lg:leading-none text-link"><?= $menu_item['title']; ?></a>
					<?php if ( ! empty( $menu_item['children'] ) ) : ?>
						<span class="icon-wrapper ml-4 w-8 h-8 lg:mb-1 lg:w-5 lg:h-5 flex justify-center">
							<svg aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="chevron w-full h-auto"><path fill="currentColor" d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"></path></svg>
						</span>
					<?php endif; ?>
				</span>

					<?php if ( ! empty( $menu_item['children'] ) ) : ?>
						<div class="submenu flex flex-col bg-gray-500 shadow-inner lg:bg-white lg:shadow-md lg:border-t-8 lg:border-solid lg:border-lime-500">
							<?php foreach ( $menu_item['children'] as $submenu_item ) : ?>
								<p class="pl-8  md:pl-16 lg:pl-8 first:pt-8 last:pb-8 lg:pb-8 lg:last:pb-8">
									<a href="<?= $submenu_item['url']; ?>" <?= ( $submenu_item['target'] ) ? 'target="' . $submenu_item['target'] . '"' : null; ?> class="lg:text-black lg:hover:opacity-75 <?= $submenu_item['classes']; ?>"><?= $submenu_item['title']; ?></a>
								</p>
							<?php endforeach ?>
						</div>
					<?php endif; ?>
			</li>
		<?php endforeach; ?>
	</ul>

<?php endif; ?>
