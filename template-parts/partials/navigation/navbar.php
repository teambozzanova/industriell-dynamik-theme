<?php
/**
 * The navbar template file.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @since   1.0.0
 */

namespace Industrielldynamik;

?>

<nav class="navbar font-poppins tracking-wider z-40 w-full text-white <?= $primary_navigation_class ?? null; ?>" role="navigation" aria-label="Primary navigation">
	<section class="container flex items-end flex-row h-full px-8 md:px-16">

		<?= get_theme_template( 'template-parts/partials/navigation/primary_navigation', ['primary_navigation' => $primary_navigation] ); ?>

		<ul class="order-2 ml-auto my-auto lg:my-0">
			<li role="none" class="cursor-pointer menu-item bg-transparent lg:relative">
				<span class="pt-4 pb-4 lg:pb-10 pr-8 pl-4 h-full flex items-end lg:justify-center lg:hover:opacity-75 inline-flex">
					<a href="<?= esc_url( home_url( '/?s= ' ) ); ?>" id="navbar-search-btn" role="menuitem" class="font-light leading-none flex flex-row items-end">
						<span class="icon-wrapper w-10 h-10 lg:ml-4 lg:w-8 lg:h-8 mr-4 lg:w-7 lg:h-7 flex justify-center">
							<svg aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="20" height="20" class="search w-full h-auto"><path fill="#fff" d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z"></path></svg>
						</span>
						<span class="hidden lg:flex"><?= __( 'Search', 'industrielldynamik' ); ?></span>
					</a>
				</span>
			</li>
		</ul>

		<?= get_theme_template( 'template-parts/partials/navigation/hamburger_button', ['primary_navigation' => $primary_navigation] ); ?>

		<?= get_theme_template( 'template-parts/partials/logo/header_logo', ['logo' => registry_get( 'logo' )->get_custom_logo()] ); ?>

	</section>
</nav>


