<li role="none" class="lg:hidden">
	<header class="pt-6 pb-6 lg:pb-10 px-8 md:px-16 lg:px-8 border-b border-solid border-gray-300 flex items-center w-full justify-between">
		<span id="navbar-close-btn" class="icon-wrapper ml-auto w-8 h-full flex justify-center cursor-pointer">
			<svg aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" width="20" height="20" class="w-full h-auto"><path fill="#FFF" d="M207.6 256l107.72-107.72c6.23-6.23 6.23-16.34 0-22.58l-25.03-25.03c-6.23-6.23-16.34-6.23-22.58 0L160 208.4 52.28 100.68c-6.23-6.23-16.34-6.23-22.58 0L4.68 125.7c-6.23 6.23-6.23 16.34 0 22.58L112.4 256 4.68 363.72c-6.23 6.23-6.23 16.34 0 22.58l25.03 25.03c6.23 6.23 16.34 6.23 22.58 0L160 303.6l107.72 107.72c6.23 6.23 16.34 6.23 22.58 0l25.03-25.03c6.23-6.23 6.23-16.34 0-22.58L207.6 256z"></path></svg>
		</span>
	</header>
</li>
