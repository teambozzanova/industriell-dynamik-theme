<?php if ( $primary_navigation ?? null ) : ?>
	<span id="hamburger-btn" class="hamburger-btn order-last ml-1 my-auto overflow-hidden cursor-pointer relative w-12 h-10 lg:hidden">
		<span class="bar-top block absolute inset-0 m-auto w-full h-1"></span>
		<span class="bar-middle block absolute inset-0 m-auto w-full h-1"></span>
		<span class="bar-bottom block absolute inset-0 m-auto w-full h-1"></span>
	</span>
<?php endif; ?>
