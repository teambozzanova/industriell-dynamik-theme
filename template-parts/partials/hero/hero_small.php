<?php
/**
 * The hero template file.
 *
 * @package industrielldynamik;
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 */

namespace Industrielldynamik;

?>

<header id="hero" class="relative flex flex-col items-center justify-center hero-small <?= $background_color ?? null; ?>">
	<?php if ( $background_image ?? null ) : ?>
		<?= get_theme_template( 'template-parts/partials/image', [ 'image' => get_responsive_image( (int) $background_image, 'landscape-lg', ['class' => "w-full h-full absolute inset-0 m-auto object-cover {$background_crop}"] ) ] ); ?>
	<?php endif; ?>

	<?php if ( $image_filter ?? null ) : ?>
		<?php if ( 'none' !== $image_filter ) : ?>
			<?= get_theme_template( 'template-parts/partials/image_filter', ['filter_class' => $image_filter, 'opacity' => (int) $image_filter_opacity] ); ?>
		<?php endif; ?>
	<?php endif; ?>

	<?php if ( $content ?? null ) : ?>
		<section class="container w-full px-8 md:px-16 py-24 relative flex flex-col lg:flex-row">
			<div class="w-full lg:w-2/3 content lg:pr-16">
				<?= $content; ?>
			</div>

			<?php if ( $sidebar ?? null ) : ?>
				<div class="w-full lg:w-1/3">
					<?= $sidebar; ?>
				</div>
			<?php endif; ?>

		</section>
	<?php endif; ?>
</header>
