<?php
/**
 * The hero sidebar template file.
 *
 * @package industrielldynamik;
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 */

namespace Industrielldynamik;

$active_category = isset( $_GET['cat'] ) ? sanitize_key( $_GET['cat'] ) : null;

$active_category_override = $active_category_override ?? null;

if ( $active_category_override ) {
	$active_category = $active_category_override;
}

?>

<aside id="hero-sidebar" class="mt-16 lg:mt-0 relative" aria-expanded="<?= ( $active_category && ! $active_category_override ) ? 'true' : 'false'; ?>">
	<?php if ( $title ?? null ) : ?>
		<header class="bg-gray-500 px-12 py-6 flex flex-row text-white">
			<h6 class="text-h5 pb-0 font-semibold font-poppins"><?= $title; ?></h6>
			<span id="sidebar-hamburger-btn" class="hamburger-btn my-auto ml-auto overflow-hidden cursor-pointer relative w-12 h-10 lg:hidden">
				<span class="bar-top block absolute inset-0 m-auto w-full h-1"></span>
				<span class="bar-middle block absolute inset-0 m-auto w-full h-1"></span>
				<span class="bar-bottom block absolute inset-0 m-auto w-full h-1"></span>
			</span>
		</header>
	<?php endif; ?>
	<?php if ( $categories ?? null ) : ?>
		<div class="bg-gray-100 px-12 py-8 w-full absolute left-0 right-0 lg:static lg:left-auto lg:right-auto z-10">
			<ul>
				<?php foreach ( $categories as $category ) : ?>
					<li class="mb-4 font-poppins">
						<?= get_theme_template( 'template-parts/partials/link',
						[
							'link_type'     => 'internal',
							'link_internal' => $category['link'],
							'content'       => $category['name'] . ' (' . ( $category['count'] ?? '0' ) . ')',
							'class'         => ( $category['slug'] === $active_category || ( $category['slug'] === 'all' && $active_category === null && $active_category_override === null ) ) ? 'text-link active' : 'text-link',
						] ); ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>
</aside>
