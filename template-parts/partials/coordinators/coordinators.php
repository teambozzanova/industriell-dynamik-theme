<?php
/**
 * The featured_cards template file.
 *
 * @package industrielldynamik;
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 */

namespace Industrielldynamik;

?>

<?php if ( $coordinators_premable || $coordinators ) : ?>
	<section class="bg-white">

		<?php if ( $coordinators_premable ?? null ) : ?>
			<div class="content container px-8 md:px-16 pt-20 <?= ( ! $coordinators ) ? 'pb-32' : null; ?>">
				<?= $coordinators_premable; ?>
			</div>
		<?php endif; ?>

		<?php if ( $coordinators ?? null ) : ?>
			<div class="container px-4 md:px-8 py-20 flex flex-col md:flex-row flex-wrap">
				<?php foreach ( $coordinators as $coordinator ) : ?>
					<?= get_theme_template( 'template-parts/partials/member_cards/member_cards',
					[
						'member_thumbnail'       => $coordinator['member_thumbnail'],
						'member_name'            => $coordinator['member_name'],
						'member_title'           => $coordinator['member_title'],
						'member_organization_id' => $coordinator['member_organization_id'],
						'member_email'           => $coordinator['member_email'],
						'member_phone_number'    => $coordinator['member_phone_number'],
						'filterable'             => false,
						'columns'                => 'md:w-1/3 lg:w-1/4',
					] ); ?>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</section>
<?php endif; ?>
