<?php if ( $members ?? null ) : ?>
	<section class="bg-gray-100">
		<?php if ( $title ?? null ) : ?>
			<header class="container px-8 md:px-16 pt-20">
        <?php if(count($members) == 1): ?>
          <p class="text-h5 pb-0"><?= $title . ' - ' . __( 'Contact', 'industrielldynamik' ); ?></p>
        <?php else: ?>
				  <p class="text-h5 pb-0"><?= $title . ' - ' . __( 'Contacts', 'industrielldynamik' ); ?></p>
        <?php endif; ?>
			</header>
		<?php endif; ?>
		<div class="container px-4 md:px-8 pb-20 flex flex-row flex-wrap <?= ( $title ) ? 'pt-12' : 'pt-20' ?>">
			<?php foreach ( $members as $member ) : ?>
				<?= $member; ?>
			<?php endforeach; ?>
		</div>
	</section>
<?php endif; ?>
