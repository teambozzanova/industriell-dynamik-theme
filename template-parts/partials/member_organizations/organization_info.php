<?php if ( $member_organization_name || $member_organization_street_address || $member_organization_postal_code || $member_organization_postal_address || $member_organization_email || $member_organization_phone || $member_organization_homepage || $member_organization_logo ) : ?>
	<section class="bg-white">
		<div class="container px-8 md:px-16 flex flex-row flex-wrap">
		<aside id="organization-info" class="max-w-full w-full md:w-2/3">
					<hr>
						<div class="py-8 w-full flex flex-col sm:flex-row">

							<ul class="mb-16">
								<?php if ( $member_organization_name ?? null ) : ?>
									<li class="mb-2">
										<p class="font-semibold text-h6 pb-0"><?= $member_organization_name; ?></p>
									</li>
								<?php endif; ?>

								<?php if ( $member_organization_street_address || $member_organization_postal_code || $member_organization_postal_address ) : ?>
									<li class="mb-2">
										<?= $member_organization_street_address.'<br>' ?? null; ?>
                    <?= $member_organization_postal_code ?? null; ?> <?= $member_organization_postal_address ?? null; ?>
									</li>
								<?php endif; ?>

								<?php if ( $member_organization_phone ?? null ) : ?>
									<li class="mb-2">
										<span class="font-semibold"><?= __( 'Tel:', 'industrielldynamik' ); ?></span> <a href="tel:<?= $member_organization_phone; ?>" class="text-link"><?= $member_organization_phone; ?></a>
									</li>
								<?php endif; ?>

								<?php if ( $member_organization_email ?? null ) : ?>
									<li class="mb-2">
										<span class="font-semibold"><?= __( 'E-mail:', 'industrielldynamik' ); ?></span> <a href="mailto:<?= $member_organization_email; ?>" class="text-link"><?= $member_organization_email; ?></a>
									</li>
								<?php endif; ?>

								<?php if ( $member_organization_homepage ?? null ) : ?>
									<li class="mb-2">
										<span class="font-semibold"><?= __( 'Web:', 'industrielldynamik' ); ?></span> <a href="<?= $member_organization_homepage; ?>" class="text-link"><?= preg_replace( "#^[^:/.]*[:/]+#i", "", urldecode( $member_organization_homepage ) ); ?></a>
									</li>
								<?php endif; ?>
							</ul>

							<?php if ( $member_organization_logo ?? null ) : ?>
								<div class="sm:ml-auto mt-6 sm:mt-0">
									<figure class="relative overflow-hidden inline-block">
											<?= $member_organization_logo; ?>
									</figure>
								</div>
							<?php endif; ?>

						</div>
				</aside>
		</div>
	</section>
<?php endif; ?>
