<?php
/**
 * The member organizations template file.
 *
 * @package industrielldynamik;
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://codex.wordpress.org/Template_Hierarchy
 * @since   1.0.0
 */

namespace Industrielldynamik;

?>

<?php if ( $preamble || $member_organizations ) : ?>
	<section class="bg-white">
		<?php if ( $preamble ?? null ) : ?>
		<div class="member-organizations-preamble content container px-8 md:px-16 pt-20 <?= ( ! $member_organizations ) ? 'pb-20' : null; ?>">
			<?= $preamble; ?>
		</div>
		<?php endif; ?>

		<?php if ( $member_organizations ?? null ) : ?>
		<div class="container px-2 md:px-10 pb-32 flex flex-row flex-wrap <?= ( ! $preamble ) ? 'pt-20' : 'pt-16'; ?>">
			<?php foreach ( $member_organizations as $member_organization ) : $member_organization = registry_get( 'member_organizations' )->get_member_organization_by_id( $member_organization['member_organization_id'] ); ?>
				<?php if ( isset( $member_organization['member_organization_thumbnail']['src'] ) ) : ?>
				<article class="px-6 py-1 w-full sm:w-1/2 md:w-1/3 lg:w-1/4 <?= ( isset( $member_organization['member_organization_link'] ) ) ? 'transition-opacity duration-500 ease-linear opacity-100 hover:opacity-50' : null; ?>">
					<a href="<?= $member_organization['member_organization_link'] ?? '#'; ?>">
						<figure class="relative pb-9_16">
							<?= get_theme_template( 'template-parts/partials/image', [ 'image' => $member_organization['member_organization_thumbnail'] ] ); ?>
						</figure>
					</a>
				</article>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>

	</section>
<?php endif; ?>
