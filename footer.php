<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the main-tag.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @since   1.0.0
 */

namespace Industrielldynamik;

?>

	</main>
		<?php do_action( 'after_main' ); ?>
		<?php wp_footer(); ?>
	</body>
</html>
