<?php
/* Template Name: Kontaktsida */

/**
 * The contacts page template file.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#front-page-display
 * @since   1.0.0
 */

namespace Industrielldynamik;

use function get_footer;
use function get_header;

$coordinators_premable = registry_get( 'acf' )->get_field( 'coordinators_premable' );
$coordinators          = registry_get( 'acf' )->get_field( 'coordinators' );

if ( $coordinators && is_array( $coordinators ) ) {
	$coordinators = array_map( function( $coordinator ) {
		return registry_get( 'members' )->get_member_by_id( $coordinator['coordinator_id'] );
	}, $coordinators );
}

$advisors_premable     = registry_get( 'acf' )->get_field( 'advisors_premable' );
//$advisors              = registry_get( 'acf' )->get_field( 'advisors' );
$advisors              = registry_get( 'members' )->get_member_items();


if ( $advisors && is_array( $advisors ) ) {
	$advisors = array_map( function( $advisor ) {
		//echo '<pre>'; print_r( $advisor ); echo '</pre>';
		return registry_get( 'members' )->get_member_by_id( $advisor['post_id'] );
		//return registry_get( 'members' )->get_member_by_id( $advisor['advisor_id'] );
	}, $advisors );
}

?>

<?php get_header(); ?>

<?php if ( $coordinators_premable || $coordinators ) : ?>
	<?= get_theme_template( 'template-parts/partials/coordinators/coordinators', ['coordinators_premable' => $coordinators_premable, 'coordinators' => $coordinators] ); ?>
<?php endif; ?>

<?php if ( $advisors_premable || $advisors ) : ?>
	<?= get_theme_template( 'template-parts/partials/advisors/advisors', ['advisors_premable' => $advisors_premable, 'advisors' => $advisors] ); ?>
<?php endif; ?>

<?php get_footer(); ?>
