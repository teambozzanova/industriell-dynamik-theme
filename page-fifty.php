<?php /* Template Name: Tjänstemall */ 


namespace Industrielldynamik;

use function get_footer;
use function get_header;

?>

<?php get_header(); ?>


	<div class="container px-8 md:px-16 py-24">
		<article class="fifty-article content">

			<?php if( have_rows('row_two_columns') ): ?>
                    
                    <?php while( have_rows('row_two_columns') ): the_row(); ?>
                        <section class="fifty-section">
                        <div class="column-1">

                            <?php if( get_sub_field('column_1') ): ?>
                                <?php the_sub_field('column_1'); ?>
                            <?php endif; ?>

                            <?php if( get_sub_field('iframe_video_1') ): ?>
                                <div class="responsive-video"><?php the_sub_field('iframe_video_1'); ?></div>
                            <?php endif; ?>

                            <?php if( have_rows('cards_1') ): ?>
                                <div class="cards">
                                <?php while( have_rows('cards_1') ): the_row(); ?>
                                    <div class="card">
                                        <?php if( get_sub_field('namn_1') ): ?>
                                            <h4><?php the_sub_field('namn_1'); ?></h4>
                                        <?php endif; ?>
                                        <?php if( get_sub_field('titel_1') ): ?>
                                            <p class="title"><?php the_sub_field('titel_1'); ?></p>
                                        <?php endif; ?>
                                        <?php if( get_sub_field('e-post_1') ): ?>
                                            <p><a href="maito:<?php the_sub_field('e-post_1'); ?>"><?php the_sub_field('e-post_1'); ?></a></p>
                                        <?php endif; ?>
                                        <?php if( get_sub_field('telefon_1') ): ?>
                                            <p><a href="tel:<?php the_sub_field('telefon_1'); ?>"><?php the_sub_field('telefon_1'); ?></a></p>
                                        <?php endif; ?>
                                        <?php if( get_sub_field('linkedin_1') ): ?>
                                            <p><a href="<?php the_sub_field('linkedin_1'); ?>">LinkedIn</a></p>
                                        <?php endif; ?>
                                    </div>
                                <?php endwhile; ?>
                                </div>
                            <?php endif; ?>
                        </div>  

                        <div class="column-2">

                            <?php if( get_sub_field('column_2') ): ?>
                                <?php the_sub_field('column_2'); ?>
                            <?php endif; ?>

                            <?php if( get_sub_field('iframe_video_2') ): ?>
                                <div class="responsive-video"><?php the_sub_field('iframe_video_2'); ?></div>
                            <?php endif; ?>

                            <?php if( have_rows('cards_2') ): ?>
                                <div class="cards">
                                <?php while( have_rows('cards_2') ): the_row(); ?>
                                    <div class="card">
                                        <?php if( get_sub_field('namn_2') ): ?>
                                            <h4><?php the_sub_field('namn_2'); ?></h4>
                                        <?php endif; ?>
                                        <?php if( get_sub_field('titel_2') ): ?>
                                            <p class="title"><?php the_sub_field('titel_2'); ?></p>
                                        <?php endif; ?>
                                        <?php if( get_sub_field('e-post_2') ): ?>
                                            <p><a href="maito:<?php the_sub_field('e-post_2'); ?>"><?php the_sub_field('e-post_2'); ?></a></p>
                                        <?php endif; ?>
                                        <?php if( get_sub_field('telefon_2') ): ?>
                                            <p><a href="tel:<?php the_sub_field('telefon_2'); ?>"><?php the_sub_field('telefon_2'); ?></a></p>
                                        <?php endif; ?>
                                        <?php if( get_sub_field('linkedin_2') ): ?>
                                            <p><a href="<?php the_sub_field('linkedin_2'); ?>">LinkedIn</a></p>
                                        <?php endif; ?>
                                    </div>
                                <?php endwhile; ?>
                                </div>
                            <?php endif; ?>
                        </div>  
                        </section>
                    <?php endwhile; ?>
                    
                <?php endif; ?>
			
		</article>
	</div>


<?php get_footer(); ?>
