<?php
/**
 * The template for displaying all single posts.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 * @since   1.0.0
 */

namespace Industrielldynamik;

use function get_footer;
use function get_header;

?>

<?php get_header(); ?>

<section class="bg-white">
	<div class="container px-8 md:px-16 py-24 flex flex-wrap">
		<article class="content w-full max-w-full lg:w-2/3 gutenberg">
			<?= get_theme_template('template-parts/layouts/content/content'); ?>
		</article>
			<?= get_theme_template('template-parts/layouts/content/aside'); ?>
	</div>
</section>

<?php get_footer(); ?>
