# Copyright (C) 2020 industrielldynamik
# This file is distributed under the same license as the industrielldynamik package.
msgid ""
msgstr ""
"Project-Id-Version: industrielldynamik\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../functions.php:32
msgid "theme error"
msgstr ""

#: ../header.php:30
msgid "This page requires JavaScript, please enable it in your browser to access the content."
msgstr ""

#: ../search.php:20
msgid "Search Results for"
msgstr ""

#. translators: 1: theme name, 2: required WP version number, 3: required PHP version number, 4: available WP version number, 5: available PHP version number
#: ../inc/back-compat.php:33
msgid "The theme \"%1$s\" requires at least WordPress version %2$s and PHP version %3$s. You are running versions %4$s and %5$s respectively. Please update and try again."
msgstr ""

#. translators: 1: theme name, 2: required WP version number, 3: available WP version number
#: ../inc/back-compat.php:38
msgid "The theme \"%1$s\" requires at least WordPress version %2$s. You are running version %3$s. Please update and try again."
msgstr ""

#. translators: 1: theme name, 2: required WP version number, 3: available WP version number
#: ../inc/back-compat.php:43
msgid "The theme \"%1$s\" requires at least PHP version %2$s. You are running version %3$s. Please update and try again."
msgstr ""

#: ../inc/back-compat.php:56, ../inc/back-compat.php:110
msgid "Compatibility error"
msgstr ""

#: ../inc/back-compat.php:111
msgid "There is a compatibility issue which is preventing this page from rendering properly."
msgstr ""

#: ../inc/project-autoloader.php:24
msgid "Please run the"
msgstr ""

#: ../inc/project-autoloader.php:25
msgid "composer install"
msgstr ""

#: ../inc/project-autoloader.php:26
msgid "command from the theme root directory."
msgstr ""

#: ../inc/project-autoloader.php:28
msgid "Autoloader not found."
msgstr ""

#: ../inc/project-autoloader.php:29
msgid "Dependency management error"
msgstr ""

#: ../inc/project-autoloader.php:46, ../inc/project-autoloader.php:70
msgid "Registry class missing"
msgstr ""

#: ../inc/project-autoloader.php:52
msgid "Failed to retrieve key in registry"
msgstr ""

#: ../inc/project-autoloader.php:76
msgid "Failed to store key in registry"
msgstr ""

#: ../inc/project-autoloader.php:95
msgid "File not found."
msgstr ""

#: ../inc/setup.php:107
msgid "Primary Navigation"
msgstr ""

#: ../inc/setup.php:157
msgid "full-width"
msgstr ""

#: ../inc/setup.php:158
msgid "hero"
msgstr ""
