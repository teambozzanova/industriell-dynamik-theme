��    d      <  �   \      �     �     �     �     �     �     �     �     �     �     �     	     	     	     (	     :	     L	     _	     q	     �	     �	     �	     �	  	   �	  "   �	     �	     
  	   
     '
  	   3
     =
     J
     O
     X
     h
     {
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
       
        %  	   ,     6     ?     G     M     R  	   b  o   l  �   �  u   ~     �        U   #  V   y     �     �     �  
   �  
   �     �     �  &        /     @     M     Z     g     t     �     �     �     �     �     �     �     �  	   �  	   �  	   �  	     	     
        '     6     F     T     m     �     �     �     �  $  �     �     �     �            
   +     6     S     f  	   n     x  
   �     �     �     �     �     �     �     
          7     ?  
   H  +   S  +        �  	   �  
   �  	   �     �     �     �               #     3     A     F     K     Y     w     �     �     �     �     �     �     �     �     �     �                 	      i   *  �   �  o   +     �  +   �  Y   �  y   4  	   �     �     �     �     �     �     �  "   �               *     6     B     N     Z     g     n     w     �  	   �     �  	   �  
   �  
   �  
   �  
   �  
   �     �     �     �     �          $     :     G     O     _     2      S   $   =           L   P   R   A   /                         J       `   >      '   6   5       !   G   Y   ?   9       K   %      W   I       H       E   F       )   N   _                  ]      1   \         d          Q   -   ;   +   M   "                           X          7   4            <             :          T   ,       [   (   U      Z      B      C   *       a   	   0                   #   c      @   b                          .      
   8       ^            &   D         O       3   V    /search/ Add New Add or remove All Autoloader not found. Blue button Choose from the most used Compatibility error Contact Contacts Cookies Cyan button Dark gray text Darkest blue text Darkest cyan text Darkest green text Darkest lime text Darkest magenta text Darkest sea text Dependency management error E-mail: Edit Email Me! Failed to retrieve key in registry Failed to store key in registry File not found. Follow us Gray button Gray text Green button Home I accept Light gray text Lightest gray text Lime button Magenta button Name New Not Found Not found in Trash Parent Please run the Popular Primary Navigation Produced by Bozzanova Registry class missing Sea button Search Search... Separate Showing Small Tel: Text in columns Text link The theme "%1$s" requires at least PHP version %2$s. You are running version %3$s. Please update and try again. The theme "%1$s" requires at least WordPress version %2$s and PHP version %3$s. You are running versions %4$s and %5$s respectively. Please update and try again. The theme "%1$s" requires at least WordPress version %2$s. You are running version %3$s. Please update and try again. Theme options There are currently no published There is a compatibility issue which is preventing this page from rendering properly. This page requires JavaScript, please enable it in your browser to access the content. Update View Web: White text Your query article articles command from the theme root directory. composer install landscape-lg landscape-md landscape-sm landscape-xl landscape-xs landscape-xxs member member organization member organizations member-organizations members page results. square-lg square-md square-sm square-xl square-xs square-xxs strong magenta success stories success story success story categories success story category success-stories theme error with commas yielded Project-Id-Version: industrielldynamik
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: sv_SE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 /sok/ Lägg till ny Lägga till eller ta bort Alla Autoloadern kunde inte hittas. Blå knapp Välj bland de mest använda Kompatibilitetsfel Kontakt Kontakter Cookies Cyan knapp Mörkgrå text Mörkaste blå texten Mörkaste cyantext Mörkaste, gröna texten Mörkaste limetexten Mörkaste magenta-texten Mörkaste havstext Fel vid beroendehantering E-post: Redigera Mejla mig! Det gick inte att hämta nyckel i registret Det gick inte att lagra nyckeln i registret Filen kunde inte hittas. Följ oss Grå knapp Grå text Grön knapp Hem Jag accepterar Ljusgrå text Ljusaste grå text Limegrön knapp Magenta knapp Namn Nytt Hittades inte Hittades inte i papperskorgen Överordnad Vänligen kör Populär Huvudnavigering Producerad av Bozzanova Register klassen saknas Havsgrå knapp Sök Sök… Separat Visar Liten Tel: Text i kolumner Textlänk Temat "%1$s" kräver minst PHP-version %2$s. Du kör version %3$s. Vänligen uppdatera och försök igen. Temat "%1$s" kräver minst WordPress-version %2$s och PHP-version %3$s. Du kör versioner %4$s respektive %5$s. Vänligen uppdatera och försök igen. Temat "%1$s" kräver minst WordPress-version %2$s. Du kör version %3$s. Vänligen uppdatera och försök igen. Temainställningar Det finns för närvarande inga publicerade Det finns ett kompatibilitetsproblem som förhindrar att den här sidan återges korrekt. Denna sida fodrar JavaScript, vänligen aktivera det i din webbläsare för att få tillgång till denna sidas innehåll. Uppdatera Visa Webb: Vit text Din sökning artikel artiklar kommandot från temats rotkatalog. composer install landskap-lg landskap-md landskap-sm landskap-xl landskap-xs landskap-xxs medlem nätverk nätverk natverk medlemmar sida resultat. kvadrat-lg kvadrat-md kvadrat-sm kvadrat-xl kvadrat-xs kvadrat-xxs sörk magenta goda exempel gott exempel goda exempel kategorier gott exempel kategori goda-exempel temafel med kommatecken gav 