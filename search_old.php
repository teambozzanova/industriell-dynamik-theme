<?php
/**
 * The template for displaying search results pages.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 * @since   1.0.0
 */

namespace Industrielldynamik;

use function get_footer;
use function get_header;
use function get_search_query;

$home_url          = home_url( '/' );
$query             = ( ' ' === get_search_query() ) ? '' : get_search_query();
$found_posts       = 0;
$custom_post_types = [
	registry_get( 'success_stories' )->get_plural_name(),
	registry_get( 'member_organizations' )->get_plural_name(),
];

$swp_query = get_swp_query_results( $query, $custom_post_types );

?>

<?php get_header(); ?>

<section class="bg-lime-100 flex items-center justify-center py-20">
	<div class="container px-8 md:px-16 min-h-64 w-full flex flex-col">
		<form method="get" id="searchform" class="w-full my-auto" action="<?= esc_url( $home_url ); ?>">
			<div class="flex w-full my-auto">
				<input type="text" class="search input bg-white pr-24 text-gray-900" aria-label="<?= __( 'Search...', 'industrielldynamik' ); ?>" name="s" id="s" placeholder="<?= __( 'Search...', 'industrielldynamik' ); ?>" value="<?= ( $query && $swp_query['results'] ) ? $query : null; ?>" />
				<button type="submit" class="w-40 relative bg-lime-700 hover:bg-lime-900 rounded-r-full transition-colors duration-300 ease-linear" name="submit" aria-label="<?= __( 'Search...', 'industrielldynamik' ); ?>" id="searchsubmit">
					<svg aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="absolute inset-0 w-12 h-12 m-auto pointer-events-none"><path fill="#fff" d="M508.5 468.9L387.1 347.5c-2.3-2.3-5.3-3.5-8.5-3.5h-13.2c31.5-36.5 50.6-84 50.6-136C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c52 0 99.5-19.1 136-50.6v13.2c0 3.2 1.3 6.2 3.5 8.5l121.4 121.4c4.7 4.7 12.3 4.7 17 0l22.6-22.6c4.7-4.7 4.7-12.3 0-17zM208 368c-88.4 0-160-71.6-160-160S119.6 48 208 48s160 71.6 160 160-71.6 160-160 160z"></path></svg>
				</button>
			</div>
		</form>
		<div>
			<?= __( 'Your query', 'industrielldynamik' ) . ' ' . ( ( $query ) ? "\"{$query}\"" : $query ) . ' ' . __( 'yielded', 'industrielldynamik' ) . ' ' . $swp_query['found_posts'] . ' ' . __( 'results.', 'industrielldynamik' ); ?>
		</div>
</section>

<?php if ( $swp_query['results'] ?? null ) : ?>
	<section id="search-list" class="flex flex-col items-center justify-center py-20 h-full">
	</div>
		<div class="container px-8 md:px-16 min-h-64 w-full flex flex-col list">
			<?php foreach ( $swp_query['results'] as $search_result ) : ?>
				<?= get_theme_template( 'template-parts/partials/search/search_result_card', ['search_result' => $search_result] ); ?>
			<?php endforeach; ?>
		</div>

		<div class="w-full flex flex-row">
			<ul class="pagination mx-auto"></ul>
		</div>
	</section>
<?php endif; ?>

<?php get_footer(); ?>
