<?php
/**
 * Functions and definitions, this file must be parseable by PHP 5.2.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 * @since   1.0.0
 */

// Define theme wide control variables.
define( 'MIN_PHP_VERSION', '7.1' );
define( 'MIN_WP_VERSION', '4.7.0' );

// Define theme wide paths.
define( 'ROOT_URL', get_template_directory_uri() );
define( 'ROOT_URI', get_template_directory() );

// Disable theme editor in admin.
//define( 'DISALLOW_FILE_EDIT', true );

if ( function_exists( 'theme_error' ) === false ) {
	/**
	 * Helper function for prettying up errors.
	 *
	 * @param string $message The error message body.
	 * @param string $heading The error message heading.
	 * @param string $title The page title.
	 * @param array  $options Optional arguments array.
	 */
	function theme_error( $message, $heading = '', $title = '', $options = array() ) {
		$title   = $title ?: __( 'theme error', 'industrielldynamik' );
		$heading = ( $heading ) ? $heading : $title;
		$message = "<h1>{$heading}</h1><br><br>{$message}";
		wp_die( $message, $title, $options );
	};
}

// Theme compatibility check, bail early if requirements are not met.
if ( version_compare( MIN_PHP_VERSION, phpversion(), '>=' ) === true || version_compare( MIN_WP_VERSION, get_bloginfo( 'version' ), '>=' ) === true ) {
	require ROOT_URI . '/inc/back-compat.php';
	return;
}

// Setup autoloader.
require ROOT_URI . '/inc/project-autoloader.php';


