<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://codex.wordpress.org/Creating_an_Error_404_Page
 * @since   1.0.0
 */

namespace Industrielldynamik;

get_header();

do_action( '404' );

get_footer();
