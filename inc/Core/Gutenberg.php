<?php
namespace Industrielldynamik\Core;

/**
 * Class Gutenberg
 *
 * @package industrielldynamik
 * @author "eddo81 <eduardo_jonnerstig@live.com>"
 * @license MIT https://opensource.org/licenses/MIT
 * @since   1.0.0
 */
class Gutenberg {

	/**
	 * Constructor.
	 */
	public function __construct() {
		add_action( 'after_setup_theme', [ $this, 'gutenberg_setup' ] );

		// add_action( 'enqueue_block_editor_assets', [ $this, 'block_editor_styles' ], 1, 1 );
		// add_action( 'init', [ $this, 'classic_editor_styles' ] );
	}

	public function gutenberg_setup() {

		// Gutenberg Theme Support.

		// Default Block Style.
		add_theme_support( 'wp-block-styles' );
		// Color palette.
		// Set Theme Colors.
		add_theme_support(
			'editor-color-palette',
			[
				[
					'name' => __( 'strong magenta', 'industrielldynamik' ),
					'slug' => 'strong-magenta',
					'color' => '#a156b4',
				]
			]
		);
		// Remove option for custom colors.
		add_theme_support( 'disable-custom-colors' );

		// Set Theme Gradiants.
		add_theme_support(
			'editor-gradient-presets',
			[
				[
					'name'     => __( 'Vivid cyan blue to vivid purple', 'industrielldynamik' ),
					'gradient' => 'linear-gradient(135deg,rgba(6,147,227,1) 0%,rgb(155,81,224) 100%)',
					'slug'     => 'vivid-cyan-blue-to-vivid-purple'
				]
			]
		);
		// Remove option for custom gradiants.
		add_theme_support( 'disable-custom-gradients' );

		// Set custom Font sizes.
		// Set Theme Font Sizes.
		add_theme_support(
			'editor-font-sizes',
			[
				[
					'name' => __( 'Small', 'industrielldynamik' ),
					'size' => 13,
					'slug' => 'small',
				]
			]
		);
		// Remove option for custom font sizes.
		add_theme_support( 'disable-custom-font-sizes' );

		// Allow custom line height on headings and paragraphs.
		add_theme_support( 'disable-custom-line-height' );

		// Allow other custom units (vh, vw, rem, em).
		//add_theme_support( 'custom-units' );

		// Remove Core Patterns.
		//remove_theme_support( 'core-block-patterns' );

		// Add custom editor styles.
		//add_theme_support( 'editor-styles' );

		// Add Dark Mode support.
		//add_theme_support( 'dark-editor-style' );

		// Allow to set wide and full alignment on block.
		//add_theme_support( 'align-wide' );

		// Support responsive embeds.
		add_theme_support( 'responsive-embeds' );

		// Experimental: Needs Gutenberg Plugin.
		// add_theme_support( 'experimental-custom-spacing' );
		// add_theme_support( 'experimental-link-color' );
	}


	/**
	 * Block Editor Styles
	 *
	 * @return void
	 */
	public function block_editor_styles() {

		// Enqueue the editor styles.
		// wp_enqueue_style( 'twentytwenty-block-editor-styles', get_theme_file_uri( '/assets/css/editor-style-block.css' ), array(), wp_get_theme()->get( 'Version' ), 'all' );
		// wp_style_add_data( 'twentytwenty-block-editor-styles', 'rtl', 'replace' );

		// Add inline style from the Customizer.
		// wp_add_inline_style( 'twentytwenty-block-editor-styles', twentytwenty_get_customizer_css( 'block-editor' ) );

		// Add inline style for non-latin fonts.
		// wp_add_inline_style( 'twentytwenty-block-editor-styles', TwentyTwenty_Non_Latin_Languages::get_non_latin_css( 'block-editor' ) );

		// Enqueue the editor script.
		// wp_enqueue_script( 'twentytwenty-block-editor-script', get_theme_file_uri( '/assets/js/editor-script-block.js' ), array( 'wp-blocks', 'wp-dom' ), wp_get_theme()->get( 'Version' ), true );
	}

	/**
	 * Classic Editor Styles
	 *
	 * @return void
	 */
	public function classic_editor_styles() {

		// $classic_editor_styles = array(
		//   '/assets/css/editor-style-classic.css',
		// );

		// add_editor_style( $classic_editor_styles );
	}
}
