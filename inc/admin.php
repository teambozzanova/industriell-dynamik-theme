<?php
/**
 * Add admin-page code here.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @since   1.0.0
 */

namespace Industrielldynamik;

if ( defined('ABSPATH') === false ) {
	exit;
}

add_action( 'admin_init', function() {
	$admin_post    = $_GET['post'] ?? null;
	$admin_post_id = $_POST['post_ID'] ?? null;
	$post_id       = $admin_post ? $admin_post : $admin_post_id;
	$black_list    = ['front-page.php', 'contacts-page.php'];

	if ( ! isset( $post_id ) ) {
		return;
	}

	$template_file = get_post_meta( $post_id, '_wp_page_template', true );

	if ( in_array( $template_file, $black_list ) ) {
		remove_post_type_support( 'page', 'editor' );
	}
} );
