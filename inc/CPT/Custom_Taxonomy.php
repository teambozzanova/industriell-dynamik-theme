<?php

namespace Industrielldynamik\CPT;

use function add_action;
use function register_taxonomy;
use function __;

/**
 * Class Custom_Taxonomy
 *
 * @package industrielldynamik
 * @author "eddo81 <eduardo_jonnerstig@live.com>"
 * @license MIT https://opensource.org/licenses/MIT
 * @since   1.0.0
 */
class Custom_Taxonomy {

	/**
	 * Singular name.
	 *
	 * @var string
	 */
	protected $singular_name = '';

	/**
	 * Plural name.
	 *
	 * @var string
	 */
	protected $plural_name = '';

	/**
	 * Labels.
	 *
	 * @var array
	 */
	protected $labels;

	/**
	 * Post types.
	 *
	 * @var array
	 */
	protected $post_types;

	/**
	 * Args.
	 *
	 * @var array
	 */
	protected $args;

	/**
	 * Constructor.
	 *
	 * @param string $singular_name Singular name of the taxonomy.
	 * @param string $plural_name Plural name of the taxonomy.
	 * @param array  $post_types Array of post types to apply the taxonomy on.
	 * @param array  $args Array of options.
	 */
	public function __construct( string $singular_name, string $plural_name, array $post_types = [], array $args = [] ) {
		$this->singular_name = $singular_name;
		$this->plural_name   = $plural_name;
		$this->post_types    = $post_types;
		$this->labels        = [
			'name'                       => ucfirst( $this->plural_name ),
			'singular_name'              => ucfirst( $this->singular_name ),
			'menu_name'                  => ucfirst( $this->plural_name ),
			'parent_item'                => null,
			'parent_item_colon'          => null,
			'all_items'                  => sprintf( "%1s $this->plural_name", __( 'All', 'industrielldynamik' ) ),
			'view_item'                  => sprintf( "%1s $this->singular_name", __( 'View', 'industrielldynamik' ) ),
			'new_item_name'              => sprintf( "%1s $this->singular_name %2s", __( 'New', 'industrielldynamik' ), __( 'Name', 'industrielldynamik' ) ),
			'separate_items_with_commas' => sprintf( "%1s $this->singular_name", __( 'Separate', 'industrielldynamik' ), __( 'with commas', 'industrielldynamik' ) ),
			'add_new_item'               => sprintf( "%1s $this->singular_name", __( 'Add New', 'industrielldynamik' ) ),
			'add_or_remove_items'        => sprintf( "%1s $this->plural_name", __( 'Add or remove', 'industrielldynamik' ) ),
			'choose_from_most_used'      => sprintf( "%1s $this->plural_name", __( 'Choose from the most used', 'industrielldynamik' ) ),
			'edit_item'                  => sprintf( "%1s $this->singular_name", __( 'Edit', 'industrielldynamik' ) ),
			'update_item'                => sprintf( "%1s $this->singular_name", __( 'Update', 'industrielldynamik' ) ),
			'search_items'               => sprintf( "%1s $this->singular_name", __( 'Search', 'industrielldynamik' ) ),
			'popular_items'              => sprintf( "%1s $this->singular_name", __( 'Popular', 'industrielldynamik' ) ),
		];

		$default_args = [
			'hierarchical'      => false,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_rest'      => true,
			// 'update_count_callback' => '_update_post_term_count',
			// 'query_var'             => true,
			// 'rewrite'               => [ 'slug' => $this->slugify( $this->singular_name ) ],
		];

		$this->args           = array_merge( $default_args, $args);
		$this->args['label']  = ucfirst( $this->plural_name );
		$this->args['labels'] = $this->labels;

		add_action( 'init', function () {
			$this->create_custom_taxonomy();
		} );
	}

	/**
	 * Generate a slug for the taxonomy.
	 *
	 * @param string $string String to slugify.
	 * @return string
	 */
	private function slugify( string $string ) : string {
		return strtolower( trim( preg_replace( '/[^A-Za-z0-9-]+/', '-', $string ), '-' ) );
	}

	/**
	 * Underscore string.
	 *
	 * @param string $string String to underscore.
	 * @return string
	 */
	private function underscore( string $string ) : string {
		return str_replace( '-', '_', $this->slugify( $string ) );
	}

	/**
	 * Create custom taxonomy.
	 */
	private function create_custom_taxonomy() : void {
		register_taxonomy( $this->underscore( strtolower( $this->plural_name ) ), $this->post_types, $this->args );
	}

	/**
	 * Get singular name.
	 *
	 * @return string $this->singular_name
	 */
	public function get_singular_name( bool $raw = false ) : string {
		$res = ( true === $raw ) ? $this->singular_name : $this->underscore( $this->singular_name );
		return $res;
	}

	/**
	 * Get plural name.
	 *
	 * @return string $this->plural_name
	 */
	public function get_plural_name( bool $raw = false ) : string {
		$res = ( true === $raw ) ? $this->plural_name : $this->underscore( $this->plural_name );
		return $res;
	}

	/**
	 * Get labels.
	 *
	 * @return array $this->labels
	 */
	public function get_labels() : array {
		return $this->labels;
	}

	/**
	 * Get terms.
	 *
	 * @param array $options.
	 * @return array $terms
	 */
	public function get_terms( array $options = [] ) : array {
		$default_args = [
			'hide_empty' => true,
		];

		$terms_query = array_merge( $default_args, $options );
		$terms_query['taxonomy'] = $this->get_plural_name();
		$terms = get_terms( $terms_query );

		return $terms;
	}

	/**
	 * Get taxonomies by post id.
	 *
	 * @param int $post_id Id of post to get taxonomies for.
	 */
	public function get_taxonomies_by_post_id( int $post_id ) : array {
		$res = [];

		$taxonomies = get_the_terms( $post_id, $this->get_plural_name() );

		if ( $taxonomies && ! is_wp_error( $taxonomies ) ) {
			foreach ( $taxonomies as $taxonomy ) {
				$res[ $this->get_singular_name() ][ $taxonomy->slug ] = [
					'term_id'          => $taxonomy->term_id,
					'name'             => $taxonomy->name,
					'slug'             => $taxonomy->slug,
					'term_group'       => $taxonomy->term_group,
					'term_taxonomy_id' => $taxonomy->term_taxonomy_id,
					'description'      => $taxonomy->description,
					'parent'           => $taxonomy->parent,
					'count'            => $taxonomy->count,
				];
			}
		}

		return $res;
	}
}
