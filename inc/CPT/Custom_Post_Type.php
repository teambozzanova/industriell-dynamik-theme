<?php

namespace Industrielldynamik\CPT;

use function add_action;
use function register_post_type;
use function __;

/**
 * Class Custom_Post_Type
 *
 * @package industrielldynamik
 * @author "eddo81 <eduardo_jonnerstig@live.com>"
 * @license MIT https://opensource.org/licenses/MIT
 * @since   1.0.0
 */
class Custom_Post_Type {

	/**
	 * Singular name.
	 *
	 * @var string
	 */
	protected $post_type_name = '';

	/**
	 * Singular name.
	 *
	 * @var string
	 */
	protected $singular_name = '';

	/**
	 * Plural name.
	 *
	 * @var string
	 */
	protected $plural_name = '';

	/**
	 * Labels.
	 *
	 * @var array
	 */
	protected $labels;

	/**
	 * Args.
	 *
	 * @var array
	 */
	protected $args;

	/**
	 * Constructor.
	 *
	 * @param string $singular_name Singular name of the post type.
	 * @param string $plural_name Plural name of the post type.
	 * @param array  $args Array of options.
	 */
	public function __construct( string $posttype_name, string $singular_name, string $plural_name, array $args = [] ) {
		$this->singular_name = $singular_name;
		$this->plural_name   = $plural_name;
		$this->post_type_name = $posttype_name;

		$this->labels        = [
			'name'               => ucfirst( $this->plural_name ),
			'singular_name'      => ucfirst( $this->singular_name ),
			'menu_name'          => ucfirst( $this->plural_name ),
			'parent_item_colon'  => sprintf( "%1s $this->singular_name", __( 'Parent', 'industrielldynamik' ) ),
			'all_items'          => sprintf( "%1s $this->plural_name", __( 'All', 'industrielldynamik' ) ),
			'view_item'          => sprintf( "%1s $this->singular_name", __( 'View', 'industrielldynamik' ) ),
			'add_new_item'       => sprintf( "%1s $this->singular_name", __( 'Add New', 'industrielldynamik' ) ),
			'add_new'            => __( 'Add New', 'industrielldynamik' ),
			'edit_item'          => sprintf( "%1s $this->singular_name", __( 'Edit', 'industrielldynamik' ) ),
			'update_item'        => sprintf( "%1s $this->singular_name", __( 'Update', 'industrielldynamik' ) ),
			'search_items'       => sprintf( "%1s $this->singular_name", __( 'Search', 'industrielldynamik' ) ),
			'not_found'          => __( 'Not Found', 'industrielldynamik' ),
			'not_found_in_trash' => __( 'Not found in Trash', 'industrielldynamik' ),
		];

		$default_args = [
			'supports'            => ['title', 'editor', 'revisions', 'custom-fields', 'thumbnail'],
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'show_in_rest'        => true,
			'menu_position'       => 5,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		];

		$this->args           = array_merge( $default_args, $args);
		$this->args['label']  = ucfirst( $this->plural_name );

		if ( isset( $this->args['labels'] ) ) {
			$this->args['labels'] = array_merge( $this->args['labels'], $this->labels );
		}

		add_action( 'init', function () {
			$this->create_custom_post_type();
		} );
	}

	/**
	 * Create custom posttype.
	 */
	private function create_custom_post_type() : void {
		register_post_type( $this->get_posttype_name(), $this->args );
	}

	/**
	 * Get singular name.
	 *
	 * @return string $this->singular_name
	 */
	public function get_posttype_name( bool $raw = false ) : string {
		return $this->post_type_name;
	}

	/**
	 * Get singular name.
	 *
	 * @return string $this->singular_name
	 */
	public function get_singular_name( bool $raw = false ) : string {
		$res = ( true === $raw ) ? $this->singular_name : $this->underscore( $this->singular_name );
		return $res;
	}

	/**
	 * Get plural name.
	 *
	 * @return string $this->plural_name
	 */
	public function get_plural_name( bool $raw = false ) : string {
		$res = ( true === $raw ) ? $this->plural_name : $this->underscore( $this->plural_name );
		return $res;
	}

	/**
	 * Get labels.
	 *
	 * @return array $this->labels
	 */
	public function get_labels() : array {
		return $this->labels;
	}

	/**
	 * Generate a slug for the custom post type.
	 *
	 * @param string $string String to slugify.
	 * @return string
	 */
	private function slugify( string $string ) : string {
		return strtolower( trim( preg_replace( '/[^A-Za-z0-9-]+/', '-', $string ), '-' ) );
	}

	/**
	 * Underscore string.
	 *
	 * @param string $string String to underscore.
	 * @return string
	 */
	private function underscore( string $string ) : string {
		return str_replace( '-', '_', $this->slugify( $string ) );
	}
}
