<?php

namespace Industrielldynamik\Plugins;

use SWP_Query;

/**
 * Class SWP
 *
 * @package App
 * @author "eddo81 <eduardo_jonnerstig@live.com>"
 * @license MIT https://opensource.org/licenses/MIT
 * @since   1.0.0
 */
class SWP {
	/**
	 * Control variable used to determine the active status of the plugin.
	 *
	 * @var bool $is_plugin_active
	 */
	private $is_plugin_active = false;

	/**
	 * Variable used to store class options.
	 *
	 * @var array $options
	 */
	private $options = [];

	/**
	 * Constructor
	 *
	 * @var array $options Class options.
	 */
	public function __construct( array $options = [] ) {
		$this->set_plugin_status();

		if ( $this->is_plugin_active ) {
			$defaults      = [];
			$this->options = array_merge($defaults, $options);
		}
	}

	/**
	 * Set the active staatus of the class.
	 */
	private function set_plugin_status() : void {
		$active_plugins     = apply_filters( 'active_plugins', get_option('active_plugins') );
		$plugin_entrypoints = ['searchwp/searchwp.php'];

		$this->is_plugin_active = ( count( array_intersect( $active_plugins, $plugin_entrypoints) ) ) ? true : false;
	}

	/**
	 * Run SWP query
	 *
	 * @var array $args Query arguments.
	 *
	 * @return array $result
	 */
	public function run_query( array $args ) : SWP_Query {
		$result = [];

		if ( $this->is_plugin_active ) {
			$result = new SWP_Query( $args );
		}

		return $result;
	}
}
