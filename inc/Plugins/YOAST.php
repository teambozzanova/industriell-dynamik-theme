<?php

namespace Industrielldynamik\Plugins;

use function yoast_breadcrumb;
use function add_action;
use function apply_filters;
use function get_option;

/**
 * Class YOAST
 *
 * @package App
 * @author "eddo81 <eduardo_jonnerstig@live.com>"
 * @license MIT https://opensource.org/licenses/MIT
 * @since   1.0.0
 */
class YOAST {
	/**
	 * Control variable used to determine the active status of the plugin.
	 *
	 * @var bool $is_plugin_active
	 */
	private $is_plugin_active = false;

	/**
	 * Variable used to store class options.
	 *
	 * @var array $options
	 */
	private $options = [];

	/**
	 * Constructor
	 *
	 * @param array $options Yoast options.
	 * @var array $options Class options.
	 */
	public function __construct( array $options = [] ) {
		$this->set_plugin_status();

		if ( $this->is_plugin_active ) {
			$defaults = [];
			$this->options = array_merge( $defaults, $options );
			add_action( 'init', [$this, 'init'], 99 );
		}
	}

	private function set_plugin_status() : void {
		$active_plugins = apply_filters( 'active_plugins', get_option( 'active_plugins' ) );
		$plugin_entrypoints = [
			'wordpress-seo-premium/wp-seo-premium.php',
			'wordpress-seo/wp-seo.php',
		];

		$this->is_plugin_active = ( count( array_intersect( $active_plugins, $plugin_entrypoints ) ) ) ? true : false;
	}

	public function init() {
		add_filter( 'wpseo_metabox_prio', function() {
			return 'low';
		} );
	}

	public function breadcrumbs( string $id = 'breadcrumbs', string $class = '' ) {
		if ( $this->is_plugin_active && function_exists( 'yoast_breadcrumb' ) ) {
			yoast_breadcrumb( '<p id="' . $id . '" class="' . $class . '">', '</p>' );
		}
	}
}
