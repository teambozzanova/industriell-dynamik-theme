<?php

namespace Industrielldynamik\Plugins;

use function add_filter;

/**
 * Class ACF.
 *
 * @package industrielldynamik
 * @author "eddo81 <eduardo_jonnerstig@live.com>"
 */
class ACF {
	/**
	 * Control variable used when checking if ACF is active.
	 *
	 * @var bool
	 */
	private $is_acf_active = true;

	/**
	 * Control variable used to store options passed to the class.
	 *
	 * @var array
	 */
	private $options = [];

	/**
	 * Register default hooks and actions for WordPress
	 *
	 * @param array $options ACF options.
	 */
	public function __construct( array $options = [] ) {
		$defaults = [
			'acf_json_directory' => '',
			'nbsp_to_br'         => false,
		];

		$this->options       = array_merge( $defaults, $options );
		$this->is_acf_active = class_exists( 'acf' );

		if ( $this->is_acf_active ) {

			// Set Save and Load paths if ACF JSON directory is found.
			if ( file_exists($this->options['acf_json_directory']) ) {
				add_filter( 'acf/settings/save_json', [$this, 'acf_json_save_point'] );
				add_filter( 'acf/settings/load_json', [$this, 'acf_json_load_point'] );
			}
		}
	}

	/**
	 * Set the save point for ACF json files.
	 *
	 * @param string $path path to the save point.
	 */
	public function acf_json_save_point( string $path ) : string {
		$path = $this->options['acf_json_directory'];
		return $path;
	}

	/**
	 * Set the loading point for ACF json files.
	 *
	 * @param array $paths paths of the load point.
	 */
	public function acf_json_load_point( array $paths ) : array {
		unset($paths[0]);
		$paths[] = $this->options['acf_json_directory'];
		return $paths;
	}

	/**
	 * Get field.
	 *
	 * @param string $selector The field name or field key.
	 * @param mixed  $post_id The post ID where the value is saved. Defaults to the current post.
	 * @param bool   $format_value Whether to apply formatting logic. Defaults to true.
	 */
	public function get_field( string $selector, $post_id = null, bool $format_value = true ) {
		global $post;

		$default_post_id = null;

		if ( $post ) {
			$default_post_id = $post->ID;
		}

		$post_id = $post_id ?? $default_post_id;
		$result  = null;

		if ( function_exists( 'get_field' ) === true) {
			$result = get_field( $selector, $post_id, $format_value );
		}

		return $result;
	}

	/**
	 * The field.
	 *
	 * @param string $selector The field name or field key.
	 * @param mixed  $post_id The post ID where the value is saved. Defaults to the current post.
	 * @param bool   $format_value Whether to apply formatting logic. Defaults to true.
	 */
	public function the_field( string $selector, $post_id = null, bool $format_value = true ) {

		if ( function_exists( 'the_field' ) === true ) {
			echo $this->get_field($selector, $post_id, $format_value);
		}
	}

	/**
	 * Replace non-breaking white space with <br> tag.
	 *
	 * @param string $content The contents to replace.
	 * @return string
	 */
	protected function nbsp_to_br( string $content ) : string {
		$content = force_balance_tags( $content );
		$content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '<br>', $content );
		$content = preg_replace( '~\s?<p>(\s|&nbsp;)+</p>\s?~', '<br>', $content );
		return $content;
	}

	/**
	 * Create Site option pages
	 *
	 * Fixed for Polylang
	 *
	 * @param array $pages Option pages.
	 * @return void
	 */
	public function create_option_page( $pages = [] ) : void {
		$langs = '';

		if ( function_exists( 'is_plugin_active' ) === false ) {
			require ABSPATH . 'wp-admin/includes/plugin.php';
		}

		if ( is_plugin_active( 'polylang/polylang.php' || 'polylang-pro/polylang-pro.php' ) === true ) {
			$poly  = get_option( 'polylang' );
			$langs = get_option( '_transient_pll_languages_list' );
		}

		if ( function_exists( 'acf_add_options_page' ) === true ) {

			foreach ( $pages as $page ) {

				$args = [
					'page_title' => $page['page_title'],
					'menu_title' => $page['menu_title'],
					'menu_slug'  => $page['menu_slug'],
					'capability' => 'edit_posts',
					'redirect'   => false,
				];

				if ( $page['position'] ?? null ) {
					$args['position'] = $page['position'];
				}

				if ( $page['position'] ?? null ) {
					$args['icon_url'] = $page['icon_url'];
				}

				// is polylang installed and active.
				if ( empty( $langs ) === false ) {
					$args['post_id'] = 'options_' . $poly['default_lang'];
				}

				acf_add_options_page( $args );
			}
		}

		if ( empty( $langs ) === false ) {

			foreach ( $langs as $lang ) {

				if ( $lang['slug'] !== $poly['default_lang'] ) {

					foreach ( $pages as $page ) {
						$args = [
							'page_title' => sprintf('%s %s', $page['page_title'], $lang['name']),
							'menu_title' => sprintf('%s %s', $page['menu_title'], $lang['name']),
							'menu_slug'  => sprintf('%s-%s', $page['menu_slug'], $lang['name']),
							'capability' => 'edit_posts',
							'redirect'   => false,
							'post_id'    => 'options_' . $lang['slug'],
							'parent'     => $page['menu_slug'],
						];

						acf_add_options_sub_page( $args );
					}
				}
			}
		}
	}
}
