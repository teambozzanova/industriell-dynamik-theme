<?php
/**
 * Add filter code here.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://developer.wordpress.org/reference/functions/add_filter/
 * @since   1.0.0
 */

namespace Industrielldynamik;

use function add_filter;
use function is_admin;

if ( defined('ABSPATH') === false ) {
	exit;
}

add_filter( 'upload_mimes', function ( $mimes ) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
});

add_filter( 'mce_buttons_2', function ( $buttons ) {
	$buttons[] = 'styleselect';
	return $buttons;
});

add_filter('tiny_mce_before_init', function( $init_array ) {

	$style_formats = [
		[
			'title'    => __( 'White text', 'industrielldynamik' ),
			'selector' => 'p,h1,h2,h3,h4,h5,h6,a,small,blockquote,ul,ol,li',
			'classes'  => 'text-white',
		],

		[
			'title'    => __( 'Darkest lime text', 'industrielldynamik' ),
			'selector' => 'p,h1,h2,h3,h4,h5,h6,a,small,blockquote,ul,ol,li',
			'classes'  => 'text-lime-500',
		],

		[
			'title'    => __( 'Darkest green text', 'industrielldynamik' ),
			'selector' => 'p,h1,h2,h3,h4,h5,h6,a,small,blockquote,ul,ol,li',
			'classes'  => 'text-green-500',
		],

		[
			'title'    => __( 'Darkest sea text', 'industrielldynamik' ),
			'selector' => 'p,h1,h2,h3,h4,h5,h6,a,small,blockquote,ul,ol,li',
			'classes'  => 'text-cyan-500',
		],

		[
			'title'    => __( 'Darkest cyan text', 'industrielldynamik' ),
			'selector' => 'p,h1,h2,h3,h4,h5,h6,a,small,blockquote,ul,ol,li',
			'classes'  => 'text-cyan-500',
		],

		[
			'title'    => __( 'Darkest blue text', 'industrielldynamik' ),
			'selector' => 'p,h1,h2,h3,h4,h5,h6,a,small,blockquote,ul,ol,li',
			'classes'  => 'text-blue-500',
		],

		[
			'title'    => __( 'Darkest magenta text', 'industrielldynamik' ),
			'selector' => 'p,h1,h2,h3,h4,h5,h6,a,small,blockquote,ul,ol,li',
			'classes'  => 'text-magenta-500',
		],

		[
			'title'    => __( 'Dark gray text', 'industrielldynamik' ),
			'selector' => 'p,h1,h2,h3,h4,h5,h6,a,small,blockquote,ul,ol,li',
			'classes'  => 'text-gray-400',
		],

		[
			'title'    => __( 'Gray text', 'industrielldynamik' ),
			'selector' => 'p,h1,h2,h3,h4,h5,h6,a,small,blockquote,ul,ol,li',
			'classes'  => 'text-gray-300',
		],

		[
			'title'    => __( 'Light gray text', 'industrielldynamik' ),
			'selector' => 'p,h1,h2,h3,h4,h5,h6,a,small,blockquote,ul,ol,li',
			'classes'  => 'text-gray-200',
		],

		[
			'title'    => __( 'Lightest gray text', 'industrielldynamik' ),
			'selector' => 'p,h1,h2,h3,h4,h5,h6,a,small,blockquote,ul,ol,li',
			'classes'  => 'text-gray-100',
		],

		[
			'title'    => __( 'Text in columns', 'industrielldynamik' ),
			'selector' => 'p,h1,h2,h3,h4,h5,h6,a,small,blockquote,ul,ol,li',
			'classes'  => 'text-columns',
		],

		[
			'title'    => __( 'Lime button', 'industrielldynamik' ),
			'selector' => 'a',
			'classes'  => 'btn btn-lime',
		],

		[
			'title'    => __( 'Green button', 'industrielldynamik' ),
			'selector' => 'a',
			'classes'  => 'btn btn-green',
		],

		[
			'title'    => __( 'Sea button', 'industrielldynamik' ),
			'selector' => 'a',
			'classes'  => 'btn btn-sea',
		],

		[
			'title'    => __( 'Cyan button', 'industrielldynamik' ),
			'selector' => 'a',
			'classes'  => 'btn btn-cyan',
		],

		[
			'title'    => __( 'Blue button', 'industrielldynamik' ),
			'selector' => 'a',
			'classes'  => 'btn btn-blue',
		],

		[
			'title'    => __( 'Magenta button', 'industrielldynamik' ),
			'selector' => 'a',
			'classes'  => 'btn btn-magenta',
		],

		[
			'title'    => __( 'Gray button', 'industrielldynamik' ),
			'selector' => 'a',
			'classes'  => 'btn btn-gray',
		],

		[
			'title'    => __( 'Text link', 'industrielldynamik' ),
			'selector' => 'a',
			'classes'  => 'text-link',
		],

	];

	if ( $style_formats ) {
		$init_array['style_formats'] = json_encode( $style_formats );
	}

	return $init_array;
});

// Whitelist gutenberg blocks.
add_filter( 'allowed_block_types', function ( $allowed_blocks ) {
	return [
		'core/image',
		'core/paragraph',
		'core/heading',
		'core/list',
		'core/quote',
		'core/shortcode',
		'core/embed',
		'core/table',
		'core/separator',
		'core/group',
	];
} );

// Make custom image sizes available in Gutenberg editor.
add_filter( 'image_size_names_choose', function ( $sizes ) {
	global $_wp_additional_image_sizes;
	$custom_image_sizes = [];

	if ( ! $_wp_additional_image_sizes ) {
		return $sizes;
	}

	foreach ( $_wp_additional_image_sizes as $image_size_name => $image_size_value ) {
		if ( array_key_exists( $image_size_name, $sizes ) === true ) {
			continue;
		}
		$custom_image_sizes[ $image_size_name ] = ucfirst ( preg_replace('/(\W|_)+/im', ' ', $image_size_name ) );
	}

	return array_merge( $sizes, $custom_image_sizes );
});

add_filter( 'script_loader_tag', function ( $tag, $handle ) {
	$theme_scrips = registry_get( 'asset_manager' )->get_registered_asset_names( 'script' );

	if ( is_admin() === true || in_array( $handle, $theme_scrips, true ) === false ) {
		return $tag;
	}

	return str_replace( ' src', ' async="async" src', $tag );
}, 90, 2 );

add_filter( 'style_loader_tag', function ( $html, $handle, $href ) {
	$theme_styles = registry_get( 'asset_manager' )->get_registered_asset_names( 'style' );

	if ( is_admin() === false && in_array( $handle, $theme_styles, true ) === false ) {
		registry_get( 'asset_manager' )->set_resource_hint( 'preload', 'text/css', $href, 'style' );
	}

	return $html;
}, 999, 3 );

add_filter( 'body_class', function( $classes ) {
	if ( is_search() ) {
		$classes[] = 'bg-gray-100';
	}
	return $classes;
} );
