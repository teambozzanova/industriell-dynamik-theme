<?php
/**
 * Global helper functions.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @since   1.0.0
 */

namespace Industrielldynamik;

use WP_Query;

if ( defined( 'ABSPATH' ) === false ) {
	exit;
}

if ( function_exists( 'get_theme_template' ) === false ) {
	/**
	 * Like ***get_template_part()*** but lets you pass args to the template file.
	 * Args are available in the template as regular variables named after their corresponding key in the ***$vars*** array.
	 * Returns the template file as a string.
	 *
	 * @param string $template_path Path to the template file.
	 * @param array  $vars Optional template variables.
	 *
	 * @return string
	 */
	function get_theme_template( $template_path, array $vars = [] ) : string {
		$template_extension = '.php';
		$template_path      = ( substr( $template_path, -4 ) !== $template_extension ) ? $template_path . $template_extension : $template_path;
		$template_file      = locate_template( [$template_path], false, false );

		if ( ! file_exists( $template_file ) ) {
			return '';
		}

		ob_start();

		if ( ! empty ($vars ) ) {
			extract( $vars );
		}

		include $template_file;
		$data = ob_get_clean();
		return $data;
	}
}

if ( function_exists( 'get_current_url' ) === false ) {
	/**
	 * Get the current url.
	 *
	 * @return string
	 */
	function get_current_url() : string {
		global $wp;
		$current_url = home_url( add_query_arg( array(), $wp->request ) ) . '/';
		return $current_url;
	}
}

if ( function_exists( 'get_svg_icon' ) === false ) {
	/**
	 * Get a svg icon.
	 *
	 * @param string $key The name of the icon to get.
	 * @return string
	 */
	function get_svg_icon( $key = '' ) : string {
		$icons = [
			'twitter'   => '<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" viewBox="0 0 448 512" height="30" width="30"><path fill="currentColor" d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-48.9 158.8c.2 2.8.2 5.7.2 8.5 0 86.7-66 186.6-186.6 186.6-37.2 0-71.7-10.8-100.7-29.4 5.3.6 10.4.8 15.8.8 30.7 0 58.9-10.4 81.4-28-28.8-.6-53-19.5-61.3-45.5 10.1 1.5 19.2 1.5 29.6-1.2-30-6.1-52.5-32.5-52.5-64.4v-.8c8.7 4.9 18.9 7.9 29.6 8.3a65.447 65.447 0 0 1-29.2-54.6c0-12.2 3.2-23.4 8.9-33.1 32.3 39.8 80.8 65.8 135.2 68.6-9.3-44.5 24-80.6 64-80.6 18.9 0 35.9 7.9 47.9 20.7 14.8-2.8 29-8.3 41.6-15.8-4.9 15.2-15.2 28-28.8 36.1 13.2-1.4 26-5.1 37.8-10.2-8.9 13.1-20.1 24.7-32.9 34z"/></svg>',
			'linkedin'  => '<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" viewBox="0 0 448 512" height="30" width="30"><path fill="currentColor" d="M416 32H31.9C14.3 32 0 46.5 0 64.3v383.4C0 465.5 14.3 480 31.9 480H416c17.6 0 32-14.5 32-32.3V64.3c0-17.8-14.4-32.3-32-32.3zM135.4 416H69V202.2h66.5V416zm-33.2-243c-21.3 0-38.5-17.3-38.5-38.5S80.9 96 102.2 96c21.2 0 38.5 17.3 38.5 38.5 0 21.3-17.2 38.5-38.5 38.5zm282.1 243h-66.4V312c0-24.8-.5-56.7-34.5-56.7-34.6 0-39.9 27-39.9 54.9V416h-66.4V202.2h63.7v29.2h.9c8.9-16.8 30.6-34.5 62.9-34.5 67.2 0 79.7 44.3 79.7 101.9V416z"></path></svg>',
			'facebook'  => '<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" viewBox="0 0 448 512" height="30" width="30"><path fill="currentColor" d="M400 32H48A48 48 0 0 0 0 80v352a48 48 0 0 0 48 48h137.25V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.27c-30.81 0-40.42 19.12-40.42 38.73V256h68.78l-11 71.69h-57.78V480H400a48 48 0 0 0 48-48V80a48 48 0 0 0-48-48z"/></svg>',
			'instagram' => '<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" viewBox="0 0 448 512" height="30" width="30"><path fill="currentColor" d="M224,202.66A53.34,53.34,0,1,0,277.36,256,53.38,53.38,0,0,0,224,202.66Zm124.71-41a54,54,0,0,0-30.41-30.41c-21-8.29-71-6.43-94.3-6.43s-73.25-1.93-94.31,6.43a54,54,0,0,0-30.41,30.41c-8.28,21-6.43,71.05-6.43,94.33S91,329.26,99.32,350.33a54,54,0,0,0,30.41,30.41c21,8.29,71,6.43,94.31,6.43s73.24,1.93,94.3-6.43a54,54,0,0,0,30.41-30.41c8.35-21,6.43-71.05,6.43-94.33S357.1,182.74,348.75,161.67ZM224,338a82,82,0,1,1,82-82A81.9,81.9,0,0,1,224,338Zm85.38-148.3a19.14,19.14,0,1,1,19.13-19.14A19.1,19.1,0,0,1,309.42,189.74ZM400,32H48A48,48,0,0,0,0,80V432a48,48,0,0,0,48,48H400a48,48,0,0,0,48-48V80A48,48,0,0,0,400,32ZM382.88,322c-1.29,25.63-7.14,48.34-25.85,67s-41.4,24.63-67,25.85c-26.41,1.49-105.59,1.49-132,0-25.63-1.29-48.26-7.15-67-25.85s-24.63-41.42-25.85-67c-1.49-26.42-1.49-105.61,0-132,1.29-25.63,7.07-48.34,25.85-67s41.47-24.56,67-25.78c26.41-1.49,105.59-1.49,132,0,25.63,1.29,48.33,7.15,67,25.85s24.63,41.42,25.85,67.05C384.37,216.44,384.37,295.56,382.88,322Z"/></svg>',
		];

		return ( true === array_key_exists( $key, $icons ) ) ? $icons[ $key ] : null;
	}
}

if ( function_exists( 'get_responsive_image' ) === false ) {
	/**
	 * Get an object containing the attributes for the custom logo.
	 *
	 * @param int    $image_id The id of the image.
	 * @param string $image_size The image size.
	 * @param array  $options The max width of the image.
	 * @return array
	 */
	function get_responsive_image( int $image_id, string $image_size = '', array $options = [] ) : array {
		$res = [
			'alt'       => null,
			'class'     => null,
			'src'       => null,
			'src_set'   => null,
			'sizes'     => null,
			'title'     => null,
			'width'     => null,
			'height'    => null,
			'mime_type' => null,
		];

		if ( '' !== $image_id ) {
			$image_alt    = get_post_meta( $image_id, '_wp_attachment_image_alt', true);
			$image_src    = wp_get_attachment_image_src( $image_id, $image_size );
			$image_srcset = esc_attr( wp_get_attachment_image_srcset( $image_id, $image_size ) );
			$image_title  = get_the_title( $image_id );
			$image_url    = $image_src[0] ? esc_url( $image_src[0] ) : null;
			$image_width  = $image_src[1] ?? null;
			$image_height = $image_src[2] ?? null;
			$image_mime   = get_post_mime_type( $image_id );
			$image_class  = $options['class'] ?? '';

			if ( $image_alt ) {
				$res['alt'] = $image_alt;
			}

			if ( $image_class ) {
				$res['class'] = $image_class;
			}

			if ( $image_url ) {
				$res['src'] = $image_url;
			}

			if ( $image_srcset ) {
				$res['src_set'] = $image_srcset;
			}

			if ( $image_srcset ) {
				$res['sizes'] = $options['sizes'] ?? '100vw';
			}

			if ( $image_title ) {
				$res['title'] = $image_title;
			}

			if ( $image_width ) {
				$res['width'] = "{$image_width}px";
			}

			if ( $image_height ) {
				$res['height'] = "{$image_height}px";
			}

			$res['ratio'] = ( $image_width && $image_height ) ? ( (int) $image_height / (int) $image_width ) : null;

			if ( $image_mime ) {
				$res['mime_type'] = $image_mime;
			}
		}
		return $res;
	}
}

if ( function_exists( 'get_success_story_categories_sidebar' ) === false ) {
	/**
	 *
	 * @return string
	 */
	function get_success_story_categories_sidebar( string $title = '', $active_category_override = null ) : string {
		$success_story_categories = registry_get( 'success_story_categories' )->get_terms();
		$success_story_archive_link = get_post_type_archive_link( registry_get( 'success_stories' )->get_plural_name() );

		$success_story_categories = ( is_array( $success_story_categories ) ) ? $success_story_categories = array_map( function ( $el ) use ( $success_story_archive_link ) {
			$res = (array) $el;
			$res['link'] =  "$success_story_archive_link?cat={$res['slug']}";
			return $res;
		}, $success_story_categories ) : [];

		if ( $success_story_categories ) {
			array_unshift( $success_story_categories, [
				'name' => __( 'All', 'industrielldynamik' ),
				'link' => $success_story_archive_link,
				'slug' => 'all',
			] );
		}

		$sidebar = get_theme_template( 'template-parts/partials/hero/category_sidebar', [
			'title'                    => $title,
			'categories'               => $success_story_categories,
			'active_category_override' => $active_category_override,
		] );

		return $sidebar;
	}
}

if ( function_exists( 'get_site_options' ) === false ) {
	/**
	 * A wrapper function for AFC option pages. Returns an associative array containing the site's option details.
	 *
	 * @return array
	 */
	function get_site_options() : array {
		$result             = [];
		$slug               = 'option';

		if ( function_exists( 'pll_current_language') ) {
			$slug = 'options_' . pll_current_language();
		}

		if ( \Nova\Core\Registry::key_exists( 'acf' ) ) {
			$current_year                 = gmdate( 'Y' );
			$primary_menu                 = registry_get( 'theme_menu' )->get_menu_by_location( 'primary' );
			$footer_preamble              = registry_get( 'acf' )->get_field( 'footer_preamble', $slug );
			$company                      = registry_get( 'acf' )->get_field( 'company_name', $slug );
			$street_address               = registry_get( 'acf' )->get_field( 'company_street_address', $slug );
			$postal_address               = registry_get( 'acf' )->get_field( 'company_postal_address', $slug );
			$postal_code                  = registry_get( 'acf' )->get_field( 'company_postal_code', $slug );
			$phone                        = registry_get( 'acf' )->get_field( 'company_phone', $slug );
			$email                        = registry_get( 'acf' )->get_field( 'company_email', $slug );
			$cookie_consent               = registry_get( 'acf' )->get_field( 'cookie_consent', $slug );
			$social_medias                = registry_get( 'acf' )->get_field( 'social_medias', $slug );
			$archive_network_hero         = registry_get( 'acf' )->get_field( 'hero_network', $slug );
			$archive_success_stories_hero = registry_get( 'acf' )->get_field( 'hero_success_stories', $slug );


			$result = [
				'primary_menu'          => $primary_menu,
				'footer_preamble'       => $footer_preamble,
				'company'               => $company,
				'street_address'        => $street_address,
				'postal_address'        => $postal_address,
				'postal_code'           => $postal_code,
				'phone'                 => $phone,
				'email'                 => $email,
				'cookie_consent'        => $cookie_consent,
				'copyright_text'        => [
					sprintf( "&copy; {$current_year} %s", "{$company} | " ),
					__( 'Produced by Bozzanova', 'industrielldynamik' ),
				],
				'social_medias'                => $social_medias,
				'archive_network_hero'         => $archive_network_hero,
				'archive_success_stories_hero' => $archive_success_stories_hero,
			];
		}

		return $result;
	}
}

if ( function_exists( 'get_swp_query_results' ) === false ) {
	/**
	 * A wrapper function for fetch SWP .
	 *
	 * @param string $query The search query string to feed into SWP Search.
	 * @return array
	 */
	function get_swp_query_results( string $query = '', array $custom_post_types = [] ) : array {
		$post_types        = ['page'];
		$found_posts       = 0;

		if ( $custom_post_types ) {
			$post_types = array_merge( $post_types, $custom_post_types );
		}

		if ( ' ' === $query ) {
			$query = '';
		}

		$args = [
			's'              => $query,
			'post_type'      => $post_types,
			'post_status'    => 'publish',
			'posts_per_page' => -1,
			'order'          => 'ASC',
		];

		if ( ! empty( $query ) ) {
			$swp_query = registry_get( 'swp' )->run_query( $args );
		} else {
			$swp_query  = new WP_Query( $args );
		}

		$all_search_results = [];

		if ( count( $swp_query->posts ?? [] ) ) {

			foreach ( $swp_query->posts as $result ) {

				if ( ! in_array( $result->post_type, $post_types ) ) {
					continue;
				}

				$result->permalink = get_permalink( $result->ID );

				if ( 'page' === $result->post_type ) {
					$result->pretty_post_type = __( 'page', 'industrielldynamik' );
				}

				if ( 'member_organizations' === $result->post_type ) {
					$result->pretty_post_type = __( 'member organizations', 'industrielldynamik' );
				}

				if ( 'success_stories' === $result->post_type ) {
					$result->pretty_post_type = __( 'success stories', 'industrielldynamik' );
				}

				$all_search_results[] = (array) $result;
			}

			// Sort the results.
			usort( $all_search_results, function( $a, $b ) {
				return strcmp( $a['post_title'], $b['post_title'] );
			});

			$found_posts = count( $all_search_results );
		}

		wp_reset_query();

		return ['results' => $all_search_results, 'found_posts' => $found_posts];
	}
}
