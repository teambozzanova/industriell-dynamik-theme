<?php
/**
 * This file handles the autoloading of theme dependencies via composer.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://getcomposer.org/doc/
 * @since   1.0.0
 */

namespace Industrielldynamik;

use function locate_template;

// Path to composer autoloader.
$composer = ROOT_URI . '/vendor/autoload.php';

// Ensure dependencies are loaded.
if ( class_exists( 'Nova\\Core\\Registry' ) === false ) {
	if ( file_exists( $composer ) === false ) {
		theme_error(
			sprintf(
				"<code>{$composer}</code><br><br>%1s <code>%2s</code> %3s",
				__( 'Please run the', 'industrielldynamik' ),
				__( 'composer install', 'industrielldynamik' ),
				__( 'command from the theme root directory.', 'industrielldynamik' )
			),
			__( 'Autoloader not found.', 'industrielldynamik' ),
			__( 'Dependency management error', 'industrielldynamik' )
		);
	}
	include_once $composer;
}

if ( function_exists( 'registry_get' ) === false ) {
	/**
	 * Get a value from the registry.
	 *
	 * @param string $key Name of key in regisistry to fetch.
	 *
	 * @return string|object|array
	 */
	function registry_get( string $key ) {
		try {
			if( class_exists( 'Nova\\Core\\Registry' ) === false ) {
				throw new \Exception( __( 'Registry class missing', 'industrielldynamik' ) );
			}
			return \Nova\Core\Registry::get( $key );
		} catch ( \Exception $e ) {
			theme_error(
				$e->getMessage(),
				__( 'Failed to retrieve key in registry', 'industrielldynamik' )
			);
		}
	}
}

if ( function_exists( 'registry_set' ) === false ) {
	/**
	 * Store a value in the registry.
	 *
	 * @param string $key Name of key in regisistry to set.
	 * @param mixed  $value The value to store in registry.
	 *
	 * @return void
	 */
	function registry_set( string $key, $value ) : void {
		try {
			if( class_exists( 'Nova\\Core\\Registry' ) === false ) {
				throw new \Exception( __( 'Registry class missing', 'industrielldynamik' ) );
			}
			\Nova\Core\Registry::set( $key, $value );
		} catch ( \Exception $e ) {
			theme_error(
				$e->getMessage(),
				__( 'Failed to store key in registry', 'industrielldynamik' )
			);
		}
	}
}

/*
 * Required files.
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed.
 */

array_map(
	function ( $file ) {
		$file = "./inc/{$file}.php";
		if ( locate_template( $file, true, true ) === false ) {
			theme_error(
				"<code>{$file}</code><br><br>",
				__( 'File not found.', 'industrielldynamik' )
			);
		}
	},
	['shims', 'helpers', 'setup', 'filters', 'admin']
);
