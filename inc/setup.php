<?php
/**
 * Setup theme.
 *
 * @package industrielldynamik
 * @license MIT https://opensource.org/licenses/MIT
 * @link    https://developer.wordpress.org/reference/hooks/after_setup_theme/
 * @since   1.0.0
 */

namespace Industrielldynamik;

if ( defined( 'ABSPATH' ) === false ) {
	exit;
}

use Nova\Core\AssetManager;
use Nova\Optimization\ImageSizer;
use Nova\Optimization\CleanWp;
use Industrielldynamik\Core\Menu;
use Industrielldynamik\Core\Logo;
use Industrielldynamik\Plugins\ACF;
use Industrielldynamik\Plugins\YOAST;
use Industrielldynamik\Plugins\SWP;
use Industrielldynamik\CPT\Custom_Post_Type;
use Industrielldynamik\CPT\Custom_Taxonomy;
use Industrielldynamik\Decorators\Member_Organization_Decorator;
use Industrielldynamik\Decorators\Success_Stories_Decorator;
use Industrielldynamik\Decorators\Member_Decorator;

use function add_action;
use function add_image_size;
use function add_theme_support;
use function is_admin;
use function load_theme_textdomain;
use function register_nav_menus;

new CleanWp();
new ImageSizer();

load_theme_textdomain( 'industrielldynamik', ROOT_URI . DIRECTORY_SEPARATOR . 'languages/' );

/**
 * Project setup
 *
 * @link https://developer.wordpress.org/reference/hooks/after_setup_theme/
 */
add_action( 'after_setup_theme', function () {

	/**
	 * Load theme text domain.
	 *
	 * @link https://developer.wordpress.org/reference/functions/load_theme_textdomain/
	 */
	load_theme_textdomain( 'industrielldynamik', ROOT_URI . DIRECTORY_SEPARATOR . 'languages/' );

	/**
	 * Enqueue theme scripts and styles.
	 *
	 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_script/
	 */
	registry_get( 'asset_manager' )->enqueue_theme_scripts_and_styles( 'theme' );

	/**
	 * Add inline script- and style-tags to document head.
	 */
	if ( is_admin() === false ) {
		add_action( 'wp_head', function() {});
	}

	/**
	 * Support navigation menus.
	 *
	 * @link https://developer.wordpress.org/reference/functions/add_theme_support/
	 */
	add_theme_support( 'menus' );

	/**
	 * Clean up markup.
	 */
	add_theme_support( 'clean-up-markup' );

	/**
	 * Disables trackbacks/pingbacks.
	 */
	add_theme_support( 'disable-trackbacks-pingbacks' );

	/**
	 * Moves all scripts to wp_footer action.
	 */
	add_theme_support( 'scripts-to-footer' );

	/**
	 * Redirects search results from /?s=query to /search/query/, converts %20 to +.
	 */
	add_theme_support( 'nice-search' );

	/**
	 * Remove version query string from all styles and scripts.
	 */
	add_theme_support( 'disable-asset-versioning' );

	add_post_type_support( 'page', 'excerpt' );

	/**
	 * Enable custom logo.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/custom-logo/
	 */
	registry_get( 'logo' )->register_logo();

	/**
	 * Enable plugins to manage the document title.
	 *
	 * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
	 */
	add_theme_support( 'title-tag' );

	/**
	 * Register navigation menus.
	 *
	 * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
	 */
	register_nav_menus([
		'primary' => __( 'Primary Navigation', 'industrielldynamik' )
	]);

	/**
	 * Enable post thumbnails.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * Enable HTML5 markup support.
	 *
	 * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
	 */
	add_theme_support( 'html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form'] );

	/**
	 * Manually select Post Formats to be supported.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'] );

	/**
	 * Enable selective refresh for widgets in customizer.
	 *
	 * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
	 */
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Support default editor block styles.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/extensibility/theme-support/
	 */
	add_theme_support( 'wp-block-styles' );

	/**
	 * Support wide alignment for editor blocks.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/extensibility/theme-support/
	 */
	add_theme_support( 'align-wide' );

	/**
	 * Add custom image sizes.
	 *
	 * @link https://developer.wordpress.org/reference/functions/add_image_size/
	 */
	add_image_size( __( 'landscape-xl', 'industrielldynamik' ), 1600, 900, true );
	add_image_size( __( 'landscape-lg', 'industrielldynamik' ), 1366, 768, true );
	add_image_size( __( 'landscape-md', 'industrielldynamik' ), 1024, 576, true );
	add_image_size( __( 'landscape-sm', 'industrielldynamik' ), 768, 432, true );
	add_image_size( __( 'landscape-xs', 'industrielldynamik' ), 640, 360, true );
	add_image_size( __( 'landscape-xxs', 'industrielldynamik' ), 480, 270, true );

	add_image_size( __( 'square-xl', 'industrielldynamik' ), 1200, 1200, true );
	add_image_size( __( 'square-lg', 'industrielldynamik' ), 1025, 1025, true );
	add_image_size( __( 'square-md', 'industrielldynamik' ), 768, 768, true );
	add_image_size( __( 'square-sm', 'industrielldynamik' ), 576, 576, true );
	add_image_size( __( 'square-xs', 'industrielldynamik' ), 480, 480, true );
	add_image_size( __( 'square-xxs', 'industrielldynamik' ), 256, 256, true );

}, 5 );


// Custom Post Types.
$success_stories = new Custom_Post_Type( 'success_stories', __( 'success story', 'industrielldynamik' ), __( 'success stories', 'industrielldynamik' ), [
	'supports'    => ['title', 'editor', 'revisions', 'custom-fields', 'excerpt', 'thumbnail'],
	'menu_icon'   => 'dashicons-awards',
	'has_archive' => true,
	'rewrite'     => [
		'slug'       => __( 'success-stories', 'industrielldynamik' ),
		'with_front' => true,
	]
] );

$member_organizations = new Custom_Post_Type( 'member_organizations', __( 'member organization', 'industrielldynamik' ), __( 'member organizations', 'industrielldynamik' ), [
	'supports'    => ['title', 'editor', 'revisions', 'custom-fields', 'excerpt', 'thumbnail'],
	'menu_icon'   => 'dashicons-networking',
	'has_archive' => true,
	'rewrite'     => [
		'slug'       => __( 'member-organizations', 'industrielldynamik' ),
		'with_front' => true,
	],
] );

$partners = new Custom_Post_Type( 
	'partners', 
	__( 'Partner', 'industrielldynamik' ), 
	__( 'Partners', 'industrielldynamik' ), 
	[
		'supports'    => ['title', 'editor', 'revisions', 'custom-fields', 'excerpt', 'thumbnail'],
		'menu_icon'   => 'dashicons-networking',
		'has_archive' => false,
		'public' => false,
		'rewrite'     => [
			'slug'       => __( 'partner', 'industrielldynamik' ),
			'with_front' => true,
		],
	] 
);


$members = new Custom_Post_Type( 'members', __( 'member', 'industrielldynamik' ), __( 'members', 'industrielldynamik' ), [
	'supports'    => ['title', 'revisions', 'custom-fields', 'thumbnail'],
	'menu_icon'   => 'dashicons-businesswoman',
	'has_archive' => false,
	'rewrite'     => [
		'slug'       => __('members', 'industrielldynamik'),
		'with_front' => true,
	],
] );

// Custom Taxonomies.
$success_story_categories = new Custom_Taxonomy( __( 'success story category', 'industrielldynamik' ), __( 'success story categories', 'industrielldynamik' ), [ $success_stories->get_plural_name() ] );

$sgeneral_search_tags = new Custom_Taxonomy( __( 'general search tag', 'industrielldynamik' ), __( 'general search tags', 'industrielldynamik' ), [ $success_stories->get_plural_name() ] );

// Register theme dependencies.
registry_set( 'asset_manager', new AssetManager( ROOT_URI . '/assets/asset_manifest.json' ) );

registry_set( 'theme_menu', new Menu() );

registry_set( 'logo', new Logo() );

registry_set( 'success_stories', new Success_Stories_Decorator( $success_stories ) );

registry_set( 'success_story_categories', $success_story_categories );

registry_set( 'member_organizations', new Member_Organization_Decorator( $member_organizations ) );

// registry_set( 'partners', new Partner_Decorator( $partners ) );

registry_set( 'members', new Member_Decorator( $members ) );

// Plugins.
registry_set( 'acf', new ACF( [ 'acf_json_directory' => ROOT_URI . '/acf-json/' ] ) );

registry_set( 'swp', new SWP() );

registry_set( 'yoast', new YOAST() );

registry_get( 'acf' )->create_option_page( [
	[
		'menu_slug'  => 'theme-option',
		'page_title' => __( 'Theme options', 'industrielldynamik' ),
		'menu_title' => __( 'Theme options', 'industrielldynamik' ),
	],
] );



add_action( 'before_main', function() {
	$site_options = get_site_options() ?? null;
	$start_page_hero = registry_get( 'acf' )->get_field( 'hero' )[0];
	$standard_hero = registry_get( 'acf' )->get_field( 'hero_small' )[0];
	$archive_network_hero = ( isset( $site_options['archive_network_hero'] ) ) ? $site_options['archive_network_hero'][0] : null;
	$archive_success_stories_hero = ( isset( $site_options['archive_success_stories_hero'] ) ) ? $site_options['archive_success_stories_hero'][0] : null;

	echo get_theme_template( 'template-parts/partials/navigation/navbar', [ 'primary_navigation' => registry_get( 'theme_menu' )->get_menu_by_location( 'primary' ), 'primary_navigation_class' => ( is_front_page() ) ? 'top-0 left-0 right-0 absolute' : 'navbar-solid' ] );

	if ( ! is_front_page() ) {
		echo get_theme_template('template-parts/partials/breadcrumbs/breadcrumbs');
	}

	if ( $start_page_hero && false === is_archive() ) {
		$start_page_hero['background_crop'] = implode(' ', array_values( $start_page_hero['background_crop'] ?? [] ) );
		echo get_theme_template( 'template-parts/partials/hero/hero', $start_page_hero );
	}

	if ( $standard_hero && false === is_archive() && ! is_search() ) {

		if ( is_single() && is_singular( registry_get( 'success_stories')->get_posttype_name() ) ) {
			$active_category_override = null;
			$term_items = registry_get( 'success_story_categories' )->get_taxonomies_by_post_id( get_the_ID() );

			if ( is_array( $term_items ) ) {
				foreach ( $term_items as $term_item ) {
					foreach ( $term_item as $term ) {
						if ( null === $active_category_override ) {
							$active_category_override = $term['slug'];
						}
					}
				}
			}

			//$standard_hero['sidebar'] = get_success_story_categories_sidebar( __( 'More examples', 'industrielldynamik' ), $active_category_override );
		}

		$standard_hero['background_crop'] = implode(' ', array_values( $standard_hero['background_crop'] ?? [] ) );
		if ( is_array($standard_hero) ) {
			echo get_theme_template( 'template-parts/partials/hero/hero_small', $standard_hero );
		}
	}

	if ( $archive_network_hero && is_post_type_archive( registry_get( 'member_organizations' )->get_posttype_name() ) ) {
		$archive_network_hero['background_crop'] = implode(' ', array_values( $archive_network_hero['background_crop'] ?? [] ) );
		if ( is_array($archive_network_hero)) {
			echo get_theme_template( 'template-parts/partials/hero/hero_small', $archive_network_hero );
		}
	}

	if ( $archive_success_stories_hero && is_post_type_archive( registry_get( 'success_stories' )->get_posttype_name() ) ) {
		//$archive_success_stories_hero['sidebar']         = get_success_story_categories_sidebar( __( 'Categories', 'industrielldynamik' ) );
		$archive_success_stories_hero['background_crop'] = implode(' ', array_values( $archive_success_stories_hero['background_crop'] ?? [] ) );
		if ( is_array($archive_success_stories_hero)) {
			echo get_theme_template( 'template-parts/partials/hero/hero_small', $archive_success_stories_hero );
		}
	}

} );

add_action( 'after_main', function() {
	$site_options = get_site_options() ?? null;

	if ( is_singular( registry_get( 'member_organizations' )->get_posttype_name() ) ) {

		if( $member_organization = registry_get( 'member_organizations' )->get_member_organization_by_id( get_the_ID() ) ) {

			$members = $member_organization['member_organization_members'] ?? null;

			//echo '<pre>'; print_r( $member_organization ); echo '</pre>';

			$member_organization_info = [
				'member_organization_logo'           => null,
				'member_organization_name'           => $member_organization['member_organization_name'] ?? null,
				'member_organization_street_address' => $member_organization['member_organization_street_address'] ?? null,
				'member_organization_postal_code'    => $member_organization['member_organization_postal_code'] ?? null,
				'member_organization_postal_address' => $member_organization['member_organization_postal_address'] ?? null,
				'member_organization_email'          => $member_organization['member_organization_email'] ?? null,
				'member_organization_phone'          => $member_organization['member_organization_phone'] ?? null,
				'member_organization_homepage'       => $member_organization['member_organization_homepage'] ?? null,
			];

			if ( $member_organization['member_organization_logo'] ) {
				$member_organization_info['member_organization_logo'] = get_theme_template( 'template-parts/partials/image', [ 'image' => $member_organization['member_organization_logo'] ] );
			}

			echo get_theme_template( 'template-parts/partials/member_organizations/organization_info', $member_organization_info );

			if ( is_array( $members ) ) {
				$members = array_map( function( $member_id ) {
					$member                 = registry_get( 'members' )->get_member_by_id( $member_id );
					$member['columns']      = 'md:w-1/2 lg:w-1/4';
					$member['member_title'] = null;
					$member                 = get_theme_template( 'template-parts/partials/member_cards/member_cards', $member );
					return $member;
				}, $members );

				echo get_theme_template( 'template-parts/partials/member_organizations/members_section', [ 'title' => $member_organization_info['member_organization_name'] ?? null, 'members' => $members ] );
			}
		}
	}

	echo get_theme_template( 'template-parts/partials/site-footer/site_footer', [
		'copyright'     => ( isset( $site_options['copyright_text'][0] ) ) ? $site_options['copyright_text'][0] : null,
		'developed_by'  => ( isset( $site_options['copyright_text'][1] ) ) ? $site_options['copyright_text'][1] : null,
		'company'       => ( isset( $site_options['company'] ) ) ? $site_options['company'] : null,
		'preamble'      => ( isset( $site_options['footer_preamble'] ) ) ? $site_options['footer_preamble'] : null,
		'social_medias' => ( isset( $site_options['social_medias'] ) ) ? $site_options['social_medias'] : null,
	] );

	echo get_theme_template( 'template-parts/partials/cookies/cookies.php', [ 'site_options' => $site_options ] );

} );

// Don't load Gutenberg-related stylesheets.
add_action( 'wp_enqueue_scripts', function() {
	wp_dequeue_style( 'wp-block-library' ); // Wordpress core
	wp_dequeue_style( 'wp-block-library-theme' ); // Wordpress core
}, 100 );
