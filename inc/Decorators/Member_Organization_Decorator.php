<?php

namespace Industrielldynamik\Decorators;

use Industrielldynamik\CPT\Custom_Post_type;

use function date_i18n;
use function get_posts;
use function get_post_thumbnail_id;
use function Industrielldynamik\get_responsive_image as get_responsive_image;

/**
 * Class Member_Organization_Decorator
 *
 * @package industrielldynamik
 * @author "eddo81 <eduardo_jonnerstig@live.com>"
 * @license MIT https://opensource.org/licenses/MIT
 * @since   1.0.0
 */
class Member_Organization_Decorator {

	/**
	 * Instance of Custom Post Type class.
	 *
	 * @var object
	 */
	protected $post_type_instance;

	/**
	 * Constructor.
	 *
	 * @param Custom_Post_Type $post_type_instance Instance of the Custom_Post_Type class.
	 */
	public function __construct( Custom_Post_Type $post_type_instance ) {
		$this->post_type_instance = $post_type_instance;
	}

	/**
	 * Get member organization meta fields.
	 *
	 * @param int $post_id The post id of the member_organization.
	 * @return array $context The post type meta data.
	 */
	private function get_member_organization_meta_data( int $post_id ) : array {
		$context = [
			'member_organization_logo'           => null,
			'member_organization_street_address' => null,
			'member_organization_postal_code'    => null,
			'member_organization_postal_address' => null,
			'member_organization_email'          => null,
			'member_organization_phone'          => null,
			'member_organization_homepage'       => null,
			'member_organization_members'        => null,
		];

		if ( function_exists( 'get_field' ) ) {
			foreach ( $context as $key => $val ) {
				$field = get_field( $key, $post_id );
				if ( $field ) {
					$context[ $key ] = $field;
				}
			}
		}

		$member_organization_logo_attr      = ( $context['member_organization_logo'] ) ? get_responsive_image( (int) $context['member_organization_logo'], 'landscape-sm', ['class' => 'member-organization-default-logo'] ) : null;
		$member_organization_thumbnail_attr = get_responsive_image( (int) get_post_thumbnail_id( $post_id ), 'landscape-sm', ['class' => 'member-organization-logo w-full h-auto absolute inset-0 m-auto flex items-center justify-center object-contain'] );

		if ( $member_organization_logo_attr ) {
			$context['member_organization_logo'] = $member_organization_logo_attr;
		}

		if ( $member_organization_thumbnail_attr ) {
			$context['member_organization_thumbnail'] = $member_organization_thumbnail_attr;
		}

		return $context;
	}

	/**
	 * Get member organization by post id.
	 *
	 * @param int $post_id The post id of the member organization.
	 * @return array $context The post type data.
	 */
	public function get_member_organization_by_id( int $post_id ) : array {
		$res       = [];
		$companies = $this->get_member_organization_items();

		foreach ( $companies as $member_organization ) {
			if ( $member_organization['post_id'] === $post_id ) {
				$res = $member_organization;
			}
		}

		return $res;
	}

	/**
	 * Get all member organizations.
	 *
	 * @param array $custom_query_options An array of query options
	 * @return array $member_organization_items An array of member organization.
	 */
	public function get_member_organization_items( array $custom_query_options = [] ) : array {
		$member_organization_items = [];

		$default_query_options = [
			'numberposts' => -1,
			'post_type'   => $this->post_type_instance->get_posttype_name(),
		];

		$query_options = array_merge( $default_query_options, $custom_query_options );

		$member_organization_items_query = get_posts( $query_options );


		if ( $member_organization_items_query ) {
			foreach ( $member_organization_items_query as $member_organization ) {
				$context                                = $this->get_member_organization_meta_data( $member_organization->ID ) ?? [];
				$context['post_id']                     = $member_organization->ID;
				$context['post_status']                 = $member_organization->post_status;
				$context['post_date']                   = $member_organization->post_date;
				$context['formated_post_date']          = ucfirst( date_i18n( 'F d, Y', strtotime( $member_organization->post_date ) ) );
				$context['slug']                        = $member_organization->post_name;
				$context['guid']                        = $member_organization->guid;
				$context['content']                     = $member_organization->post_content;
				$context['member_organization_excerpt'] = wp_trim_words( $member_organization->post_excerpt, 30, '...' );
				$context['member_organization_link']    = get_the_permalink( $member_organization->ID );
				$context['member_organization_name']    = $member_organization->post_title;
				$member_organization_items[]            = $context;
			}
		}
		return $member_organization_items;
	}

	/**
	 * Get singular name.
	 *
	 * @return string $this->singular_name
	 */
	public function get_singular_name( bool $raw = false ) : string {
		return $this->post_type_instance->get_singular_name( $raw );
	}

	/**
	 * Get plural name.
	 *
	 * @return string $this->plural_name
	 */
	public function get_plural_name( bool $raw = false ) : string {
		return $this->post_type_instance->get_plural_name( $raw );
	}

	/**
	 * Get singular name.
	 *
	 * @return string $this->singular_name
	 */
	public function get_posttype_name( bool $raw = false ) : string {
		return $this->post_type_instance->get_posttype_name();
	}
}
