<?php

namespace Industrielldynamik\Decorators;

use Industrielldynamik\CPT\Custom_Post_type;

use function date_i18n;
use function get_posts;
use function get_post_thumbnail_id;
use function Industrielldynamik\get_responsive_image as get_responsive_image;

/**
 * Class partner_Decorator
 *
 * @package industrielldynamik
 * @author "eddo81 <eduardo_jonnerstig@live.com>"
 * @license MIT https://opensource.org/licenses/MIT
 * @since   1.0.0
 */
class Partner_Decorator {

	/**
	 * Instance of Custom Post Type class.
	 *
	 * @var object
	 */
	protected $post_type_instance;

	/**
	 * Constructor.
	 *
	 * @param Custom_Post_Type $post_type_instance Instance of the Custom_Post_Type class.
	 */
	public function __construct( Custom_Post_Type $post_type_instance ) {
		$this->post_type_instance = $post_type_instance;
	}

	/**
	 * Get member organization meta fields.
	 *
	 * @param int $post_id The post id of the partner.
	 * @return array $context The post type meta data.
	 */
	private function get_partner_meta_data( int $post_id ) : array {
		$context = [
			'partner_logo'           => null,
			'partner_url'       => null,
		];

		if ( function_exists( 'get_field' ) ) {
			foreach ( $context as $key => $val ) {
				$field = get_field( $key, $post_id );
				if ( $field ) {
					$context[ $key ] = $field;
				}
			}
		}

		$partner_logo_attr      = ( $context['partner_logo'] ) ? get_responsive_image( (int) $context['partner_logo'], 'landscape-sm', ['class' => 'partner-default-logo'] ) : null;
		$partner_thumbnail_attr = get_responsive_image( (int) get_post_thumbnail_id( $post_id ), 'landscape-sm', ['class' => 'partner-logo w-full h-auto absolute inset-0 m-auto flex items-center justify-center object-contain'] );

		if ( $partner_logo_attr ) {
			$context['partner_logo'] = $partner_logo_attr;
		}

		if ( $partner_thumbnail_attr ) {
			$context['partner_thumbnail'] = $partner_thumbnail_attr;
		}

		return $context;
	}

	/**
	 * Get member organization by post id.
	 *
	 * @param int $post_id The post id of the member organization.
	 * @return array $context The post type data.
	 */
	public function get_partner_by_id( int $post_id ) : array {
		$res       = [];
		$partners = $this->get_partner_items();

		foreach ( $partners as $partner ) {
			if ( $partner['post_id'] === $post_id ) {
				$res = $partner;
			}
		}

		return $res;
	}

	/**
	 * Get all member organizations.
	 *
	 * @param array $custom_query_options An array of query options
	 * @return array $partner_items An array of member organization.
	 */
	public function get_partner_items( array $custom_query_options = [] ) : array {
		$partner_items = [];

		$default_query_options = [
			'numberposts' => -1,
			'post_type'   => $this->post_type_instance->get_posttype_name(),
		];

		$query_options = array_merge( $default_query_options, $custom_query_options );

		$partner_items_query = get_posts( $query_options );


		if ( $partner_items_query ) {
			foreach ( $partner_items_query as $partner ) {
				$context                                = $this->get_partner_meta_data( $partner->ID ) ?? [];
				$context['post_id']                     = $partner->ID;
				$context['post_status']                 = $partner->post_status;
				$context['post_date']                   = $partner->post_date;
				$context['formated_post_date']          = ucfirst( date_i18n( 'F d, Y', strtotime( $partner->post_date ) ) );
				$context['slug']                        = $partner->post_name;
				$context['guid']                        = $partner->guid;
				$context['content']                     = $partner->post_content;
				$context['partner_excerpt'] = wp_trim_words( $partner->post_excerpt, 30, '...' );
				$context['partner_link']    = get_the_permalink( $partner->ID );
				$context['partner_name']    = $partner->post_title;
				$partner_items[]            = $context;
			}
		}
		return $partner_items;
	}

	/**
	 * Get singular name.
	 *
	 * @return string $this->singular_name
	 */
	public function get_singular_name( bool $raw = false ) : string {
		return $this->post_type_instance->get_singular_name( $raw );
	}

	/**
	 * Get plural name.
	 *
	 * @return string $this->plural_name
	 */
	public function get_plural_name( bool $raw = false ) : string {
		return $this->post_type_instance->get_plural_name( $raw );
	}

	/**
	 * Get singular name.
	 *
	 * @return string $this->singular_name
	 */
	public function get_posttype_name( bool $raw = false ) : string {
		return $this->post_type_instance->get_posttype_name();
	}
}
