<?php

namespace Industrielldynamik\Decorators;

use Industrielldynamik\CPT\Custom_Post_type;

use function date_i18n;
use function get_posts;
use function get_post_thumbnail_id;
use function Industrielldynamik\get_responsive_image as get_responsive_image;

/**
 * Class Member_Decorator
 *
 * @package industrielldynamik
 * @author "eddo81 <eduardo_jonnerstig@live.com>"
 * @license MIT https://opensource.org/licenses/MIT
 * @since   1.0.0
 */
class Member_Decorator {

	/**
	 * Instance of Custom Post Type class.
	 *
	 * @var object
	 */
	protected $post_type_instance;

	/**
	 * Constructor.
	 *
	 * @param Custom_Post_Type $post_type_instance Instance of the Custom_Post_Type class.
	 */
	public function __construct( Custom_Post_Type $post_type_instance ) {
		$this->post_type_instance = $post_type_instance;
	}

	/**
	 * Get member meta fields.
	 *
	 * @param int $post_id The post id of the member.
	 * @return array $context The post type meta data.
	 */
	private function get_member_meta_data( int $post_id ) : array {
		$context = [
			'member_title'           => null,
			'member_organization_id' => null,
			'member_email'           => null,
			'member_phone_number'    => null,
		];

		$member_thumbnail_attr = get_responsive_image( (int) get_post_thumbnail_id( $post_id ), 'square-md', ['class' => 'w-full h-auto'] );

		if ( function_exists( 'get_field' ) ) {
			foreach ( $context as $key => $val ) {
				$field = get_field( $key, $post_id );
				if ( $field ) {
					$context[ $key ] = $field;
				}
			}
		}

		if ( $member_thumbnail_attr ) {
			$context['member_thumbnail'] = $member_thumbnail_attr;
		}

		return $context;
	}

	/**
	 * Get member organization by post id.
	 *
	 * @param int $post_id The post id of the member organization.
	 * @return array $context The post type data.
	 */
	public function get_member_by_id( int $post_id ) : array {
		$res       = [];
		$companies = $this->get_member_items();

		foreach ( $companies as $member ) {
			if ( $member['post_id'] === $post_id ) {
				$res = $member;
			}
		}

		return $res;
	}

	/**
	 * Get all members.
	 *
	 * @return array $member_items An array of member organization.
	 */
	public function get_member_items() : array {
		$member_items       = [];
		$member_items_query = get_posts( [
			'numberposts' => -1,
			'orderby' => 'title',
			'order' => 'ASC',
			'post_type'   => $this->post_type_instance->get_posttype_name(),
		] );

		if ( $member_items_query ) {
			foreach ( $member_items_query as $member ) {
				$context                                = $this->get_member_meta_data( $member->ID ) ?? [];
				$context['post_id']                     = $member->ID;
				$context['post_status']                 = $member->post_status;
				$context['post_date']                   = $member->post_date;
				$context['formated_post_date']          = ucfirst( date_i18n( 'F d, Y', strtotime( $member->post_date ) ) );
				$context['slug']                        = $member->post_name;
				$context['guid']                        = $member->guid;
				$context['member_name']                 = $member->post_title;
				$member_items[]                         = $context;
			}
		}
		return $member_items;
	}

	/**
	 * Get singular name.
	 *
	 * @return string $this->singular_name
	 */
	public function get_singular_name() : string {
		return $this->post_type_instance->get_singular_name();
	}

	/**
	 * Get plural name.
	 *
	 * @return string $this->plural_name
	 */
	public function get_plural_name() : string {
		return $this->post_type_instance->get_plural_name();
	}

	/**
	 * Get singular name.
	 *
	 * @return string $this->singular_name
	 */
	public function get_posttype_name( bool $raw = false ) : string {
		return $this->post_type_instance->get_posttype_name();
	}
}
