<?php

namespace Industrielldynamik\Decorators;

use Industrielldynamik\CPT\Custom_Post_type;

use function date_i18n;
use function get_posts;
use function get_post_thumbnail_id;
use function Industrielldynamik\get_responsive_image as get_responsive_image;

/**
 * Class Success_Stories_Decorator
 *
 * @package industrielldynamik
 * @author "eddo81 <eduardo_jonnerstig@live.com>"
 * @license MIT https://opensource.org/licenses/MIT
 * @since   1.0.0
 */
class Success_Stories_Decorator {

	/**
	 * Instance of Custom Post Type class.
	 *
	 * @var object
	 */
	protected $post_type_instance;

	/**
	 * Constructor.
	 *
	 * @param Custom_Post_Type $post_type_instance Instance of the Custom_Post_Type class.
	 */
	public function __construct( Custom_Post_Type $post_type_instance ) {
		$this->post_type_instance = $post_type_instance;
	}

	/**
	 * Get success story meta fields.
	 *
	 * @param int $post_id The post id of the success story.
	 * @return array $context The post type meta data.
	 */
	private function get_success_story_meta_data( int $post_id ) : array {
		$context = [];

		$success_stories_thumbnail_attr = get_responsive_image( (int) get_post_thumbnail_id( $post_id ), 'square-lg', ['class' => 'w-full h-full absolute inset-0 m-auto flex items-center justify-center object-cover'] );

		if ( function_exists( 'get_field' ) ) {
			foreach ( $context as $key => $val ) {
				$field = get_field( $key, $post_id );
				if ( $field ) {
					$context[ $key ] = $field;
				}
			}
		}

		if ( $success_stories_thumbnail_attr ) {
			$context['success_stories_thumbnail'] = $success_stories_thumbnail_attr;
		}

		return $context;
	}

	/**
	 * Get success story by post id.
	 *
	 * @param int $post_id The post id of the success story.
	 * @return array $context The post type data.
	 */
	public function get_success_story_by_id( int $post_id ) : array {
		$res           = [];
		$success_stories = $this->get_success_stories_items();

		foreach ( $success_stories as $success_story ) {
			if ( $success_story['post_id'] === $post_id ) {
				$res = $success_story;
			}
		}

		return $res;
	}

	/**
	 * Get all success stories.
	 *
	 * @param array $custom_query_options An array of query options.
	 * @return array $success_story_items An array of success story.
	 */
	public function get_success_story_items( array $custom_query_options = [] ) : array {
		$default_query_options = [
			'numberposts' => -1,
			'post_type'   =>  $this->post_type_instance->get_posttype_name(),
		];

		$query_options = array_merge( $default_query_options, $custom_query_options );

		$success_story_items       = [];
		$success_story_items_query = get_posts( $query_options );

		if ( $success_story_items_query ) {
			foreach ( $success_story_items_query as $success_story ) {
				$context                                = $this->get_success_story_meta_data( $success_story->ID ) ?? [];
				$context['post_id']                     = $success_story->ID;
				$context['post_status']                 = $success_story->post_status;
				$context['post_date']                   = $success_story->post_date;
				$context['formated_post_date']          = ucfirst( date_i18n( 'F d, Y', strtotime( $success_story->post_date ) ) );
				$context['slug']                        = $success_story->post_name;
				$context['guid']                        = $success_story->guid;
				$context['content']                     = $success_story->post_content;
				$context['success_stories_link']        = get_the_permalink( $success_story->ID );
				$context['success_stories_name']        = $success_story->post_title;
				$success_story_items[]                  = $context;
			}
		}
		return $success_story_items;
	}

	/**
	 * Get singular name.
	 *
	 * @return string $this->singular_name
	 */
	public function get_singular_name( bool $raw = false ) : string {
		return $this->post_type_instance->get_singular_name( $raw );
	}

	/**
	 * Get plural name.
	 *
	 * @return string $this->plural_name
	 */
	public function get_plural_name( bool $raw = false ) : string {
		return $this->post_type_instance->get_plural_name( $raw );
	}

	/**
	 * Get singular name.
	 *
	 * @return string $this->singular_name
	 */
	public function get_posttype_name( bool $raw = false ) : string {
		return $this->post_type_instance->get_posttype_name();
	}
}
